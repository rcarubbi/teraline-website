<?php 
class CategoriaController
{
 	function AtivarDesativar($id)
 	{

 		$categoria = new CategoriaProdutoModel();
 		$categoria->GetById($id);
 		 
 		if ($categoria->Ativo == 1)
 		{
 			$categoria->Deactivate();
 		}
 		else
 		{
 			$categoria->Activate();
 		}
 	
 	}


	public $categoriaModel;

    public $needBind = 0;
 	
 	function BindCategoria($id)
 	{
 		
 		$this->categoriaModel = new CategoriaProdutoModel();
 		$this->categoriaModel->GetById($id);
 		$this->needBind = 1;

 	}

 	function CreateOrEdit($id, $loadObjectHandler)
 	{
 		if (!is_callable($loadObjectHandler)) throw new Exception('Invalid Load object Handler');
 		$this->categoriaModel = new CategoriaProdutoModel();
 		if ($id > 0)
 		{
 			$this->categoriaModel->GetById($id);
 		}
 		$params = array($this->categoriaModel);
 		$this->categoriaModel = call_user_func_array($loadObjectHandler,$params);
 		$this->categoriaModel->Save();
 		header("Location: categorias.php"); 

 	}

 	function ShowForm()
	{
		?>
		<div class="container">
			<form name="form" method="post" action="categoriaForm.php?action=Save&id=<?php echo Util::ShowField($this->needBind, $this->categoriaModel->Id); ?>">
              <fieldset>
                <legend>Categoria</legend>
                 <div class="row">
                    <div class="span12">
                	 
                        <label for="txtNome">Nome</label>
                        <input name="txtNome" id="txtNome" type="text" class="span12" value='<?php echo Util::ShowField($this->needBind, $this->categoriaModel->Nome); ?>' placeholder="Nome da Categoria" required>
                    </div>
                     
                </div>
                  <div class="row">
                	<div class="span12 pull-right">
                		 <input type="submit" value="Salvar" class="btn btn-success" />
                	</div>
                </div>
            </fieldset>
        </form>
    </div>

		<?php 
	}


	function ShowGrid()
	{
		$categoriasList = CategoriaProdutoModel::ListarCategoriasAdmin();
		?>
		<table class="table table-striped table-hover">
			<thead>
			<tr>
				<th>Nome</th>
			</tr>
			</thead>
			<tbody>
		<?php  
		foreach($categoriasList as $item)
		{

			$icon = "icon-pause";

			if ($item->Ativo == 0)
			{
				$icon = "icon-play";
			}

			$activateButton = "<a href='categorias.php?action=activateDeactivate&id=$item->Id' class='btn btn-primary'><i class='icon-play icon-white'></i></a>";

		 	echo"<tr>
				<td>$item->Nome</td>
				<td>$item->Ativo</td>
				<td><a href='categoriaForm.php?action=edit&id=$item->Id' class='btn btn-primary'><i class='icon-pencil icon-white'></i></a></td>
				<td><a href='categorias.php?action=activateDeactivate&id=$item->Id' class='btn btn-primary'><i class='$icon icon-white'></i></a></td>
				 
			</tr>";
		 
		} ?>
			<tr>
				<td colspan="9">
					<a href='categoriaForm.php?action=new' class='btn btn-primary pull-right'><i class='icon-plus icon-white'></i></a>
				</td>
			</tr>
			</tbody>
		</table>
		<?php 
	}

}

 ?>