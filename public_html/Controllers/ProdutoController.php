<?php 



class ProdutoController
{

	public $produto;

	public $categoria;

	function RenderProdutoCodigo()
	{
		echo $this->produto->Codigo;
	}


    function GetTituloPaginaProduto()
    {
        return $this->produto->Nome . " - " . $this->categoria->Nome . " - TERALINE Industria de Móveis";
    }



	function RenderProdutoNome()
	{

		echo $this->produto->Nome;

	}



	function RenderProdutoDetalhes()
	{ ?>



		<div class="container">
                    <div class="row">
			<div class="span6">
	<figure class="span6">

			<!--<img class="fotoProduto-detalhe" src="/Util\watermarkWithResize.php?image=<?php echo str_replace("\\", "/", $this->produto->CaminhoImagem) ?>&watermark=img/watermark.png" />-->

            <img class="fotoProduto-detalhe span6" src="<?php echo "/" . $this->produto->CaminhoImagem ?>" />

				<figcaption class="span6">

				<small>foto meramente ilustrativa</small>

			</figcaption>

			</figure>

			</div>
            
            
            <?php if ($this->produto->CaminhoImagem2 !== "") { 
               
                ?>
 
            	<div class="span6">
                       
	<figure class="span6">
			<!--<img class="fotoProduto-detalhe" src="/Util\watermarkWithResize.php?image=<?php echo str_replace("\\", "/", $this->produto->CaminhoImagem2) ?>&watermark=img/watermark.png" />-->
                
            <img class="fotoProduto-detalhe span6" src="<?php echo "/" . $this->produto->CaminhoImagem2 ?>" />

				<figcaption class="span6">

				<small>foto meramente ilustrativa</small>

			</figcaption>

			</figure>

			</div>
               <?php  }?>
               
                    <?php if ($this->produto->CaminhoImagem3 !== "") { ?>
            	<div class="span6">

			<figure class="span6">

			<!--<img class="fotoProduto-detalhe" src="/Util\watermarkWithResize.php?image=<?php echo str_replace("\\", "/", $this->produto->CaminhoImagem3) ?>&watermark=img/watermark.png" />-->

            <img class="fotoProduto-detalhe span6" src="<?php echo "/" . $this->produto->CaminhoImagem3 ?>" />

			<figcaption class="span6">

				<small>foto meramente ilustrativa</small>

			</figcaption>

			</figure>

			</div>
            <?php  }?>
                    <?php if ($this->produto->CaminhoImagem4 !== "") { ?>
            	<div class="span6">

			<figure class="span6">

			<!--<img class="fotoProduto-detalhe" src="/Util\watermarkWithResize.php?image=<?php echo str_replace("\\", "/", $this->produto->CaminhoImagem4) ?>&watermark=img/watermark.png" />-->

            <img class="fotoProduto-detalhe span6" src="<?php echo "/" . $this->produto->CaminhoImagem4 ?>" />

				<figcaption class="span6">

				<small>foto meramente ilustrativa</small>

			</figcaption>

			</figure>

			</div>
            <?php  }?>
		</div>

			</div>


		<div class="accordion" id="accordionDetalhesProduto">

		  <div class="accordion-group">

		    <div class="accordion-heading">

		      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionDetalhesProduto" href="#collapsePanelMedidas">

		         Medidas

		      </a>

		    </div>

		    <div id="collapsePanelMedidas" class="accordion-body collapse in">

		      <div class="accordion-inner">

		         <?php echo str_replace("\\\"", "\"", $this->produto->Medidas) ?>

		      </div>

		    </div>

		  </div>

		  <div class="accordion-group">

		    <div class="accordion-heading">

		      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionDetalhesProduto" href="#collapsePanelPesos">

		         Pesos

		      </a>

		    </div>

		    <div id="collapsePanelPesos" class="accordion-body collapse">

		      <div class="accordion-inner">

		        <?php echo str_replace("\\\"", "\"", $this->produto->Peso) ?>

		      </div>

		    </div>

		  </div>



		  <div class="accordion-group">

		    <div class="accordion-heading">

		      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionDetalhesProduto" href="#collapsePanelOutrasEspec">

		         Outras especificações

		      </a>

		    </div>

		    <div id="collapsePanelOutrasEspec" class="accordion-body collapse">

		      <div class="accordion-inner well">

		         <?php echo str_replace("\\\"", "\"", $this->produto->OutrasEspecificacoes) ?>

		      </div>

		    </div>

		  </div>

		</div>

		 





		 <?php

		 

	}

 



	function __construct($idProduto = null)

	{

		$this->produto = new ProdutoModel();

		$this->categoria = new CategoriaProdutoModel();

		if ($idProduto !== null)

		{

			$this->produto->GetById($idProduto);

			$this->categoria->GetById($this->produto->IdCategoria);

		}

	}



    function RenderCategoriaFilter($idCategoria)

    {



        ?>

        <script type="text/javascript">

            $(document).ready(function() {

                $("#cboCategoriaFiltro").change(function() {

                    $("#frmFilter").submit();

                });



            });

        </script>

        <div class="container">

        <form id="frmFilter" name="frmFilter" action="produtos.php" method="post">

             

                

                <div class="span12">

                     <label for="cboCategoriaFiltro">Filtro por categoria:</label>

                      <select id="cboCategoriaFiltro" name="cboCategoriaFiltro">

                            <option value="0" <?php echo Util::SelectItem(true, $idCategoria, 0)  ?>>-- Todos --</option>

                              <?php foreach (CategoriaProdutoModel::ListarCategorias() as $categoria) {

                                echo "<option value=\"$categoria->Id\" " .       Util::SelectItem(true, $idCategoria, $categoria->Id) . ">$categoria->Nome</option>";

                            } ?>

                      </select>

                </div>

            

        </form>

         </div>

        <?php



    }



    function AtivarDesativar($id)

    {



        $produto = new ProdutoModel();

        $produto->GetById($id);

         

        if ($produto->Ativo == 1)

        {

            $produto->Deactivate();

        }

        else

        {

            $produto->Activate();

        }

    

    }





    function ShowGrid($idCategoriaFiltro = 0)

    {

        $produtosList = ProdutoModel::ListarProdutosAdmin($idCategoriaFiltro);

        ?>

        <script type="text/javascript">

            $(document).ready(function() {

                $(".sortable").sortable(

                    { axis: "y",

                      update: function( event, ui ) 

                      {

                          var arrProductIds = new Array();

                          var productLines = $("tr.productLine");

                          for (var i = 0; i < productLines.length; i++) {

                              arrProductIds[i] = $(productLines[i]).attr("productId-data");

                          };



                          $.ajax({        

                                type: "POST",

                                url: "../services/ReorderProductsService.php",

                                data: { productIds : JSON.stringify(arrProductIds)  }

                          });



                      }

                 });

            });

        </script>

        <table class="table table-striped table-hover">

            <thead>

            <tr>

                <th>Nome</th>

                <th>Código</th>

                <th>Categoria</th>

            </tr>

            </thead>

            <tbody class="sortable">

        <?php  

        foreach($produtosList as $item)

        {

            $icon = "icon-pause";



            if ($item->Ativo == 0)

            {

                $icon = "icon-play";

            }



            $activateButton = "<a href='usuarios.php?action=activateDeactivate&id=$item->Id' class='btn btn-primary'><i class='icon-play icon-white'></i></a>";



            echo"<tr class=\"productLine\" productId-data=\"" . $item->Id . "\">

                <td>$item->Nome</td>

                <td>$item->Codigo</td>

                <td>" . $item->Categoria->Nome . "</td>

                <td><a href='produtoForm.php?action=edit&id=$item->Id' class='btn btn-primary'><i class='icon-pencil icon-white'></i></a></td>

                <td><a href='produtos.php?action=activateDeactivate&id=$item->Id' class='btn btn-primary'><i class='$icon icon-white'></i></a></td>

                </tr>";

        } ?>

            <tr>

                <td colspan="9">

                    <a href='produtoForm.php?action=new' class='btn btn-primary pull-right'><i class='icon-plus icon-white'></i></a>

                </td>

            </tr>

            </tbody>

        </table>

        <?php 

    }



 	public $needBind = 0;

 	

 	function BindProduto($id)

 	{

 		

 		$this->produto = new ProdutoModel();

 		$this->produto->GetById($id);

 		$this->needBind = 1;



 	}



    function CreateOrEdit($id, $loadObjectHandler)

    {

        if (!is_callable($loadObjectHandler)) throw new Exception('Invalid Load object Handler');

        $this->produto = new ProdutoModel();

        if ($id > 0)

        {

            $this->produto->GetById($id);

        }

        $params = array($this->produto);

        $this->produto = call_user_func_array($loadObjectHandler,$params);

        $this->produto->Save();

        header("Location: produtos.php"); 



    }



    function ShowForm()

	{

		?>

		<div class="container">

			<form name="form" enctype="multipart/form-data" method="post" action="produtoForm.php?action=Save&id=<?php echo Util::ShowField($this->needBind, $this->produto->Id); ?>">

              <fieldset>

                <legend>Produto</legend>

                 <div class="row">

                    <div class="span6">

                        <label for="txtNome">Nome</label>

                        <input name="txtNome" id="txtNome" type="text" class="span6" value='<?php echo Util::ShowField($this->needBind, $this->produto->Nome); ?>' placeholder="Nome do produto" required>

                    </div>

                    <div class="span3">

                       <label for="txtCodigo">Código</label>

                       <input name="txtCodigo" id="txtCodigo" type="text" class="span3" value='<?php echo Util::ShowField($this->needBind, $this->produto->Codigo); ?>' placeholder="Código do produto" required>

                    </div>

                     <div class="span3">

                       <label for="txtAcabamento">Acabamento em</label>

                       <input name="txtAcabamento" id="txtAcabamento" type="text" class="span3" value='<?php echo Util::ShowField($this->needBind, $this->produto->Acabamento); ?>' placeholder="acabamento em..." required>

                    </div>

                </div>

				<div class="row">

					<div class="span12">

                        <label for="txtMedidas">Medidas</label>

                        <textarea name="txtMedidas" id="txtMedidas" style="height:200px;" class="span12 ckeditor"><?php echo Util::ShowField($this->needBind, $this->produto->Medidas); ?></textarea>

                    </div>

                </div>

                <div class="row">

					<div class="span12">

                        <label for="txtPeso">Pesos</label>

                        <textarea name="txtPeso" id="txtPeso" style="height:200px;" class="span12 ckeditor"><?php echo Util::ShowField($this->needBind, $this->produto->Peso); ?></textarea>

                    </div>

                </div>

                <div class="row">

					<div class="span12">

                        <label for="txtOutrasEspecificacoes">Outras especificações</label>

                        <textarea name="txtOutrasEspecificacoes" id="txtOutrasEspecificacoes" style="height:200px;" class="span12 ckeditor"><?php echo Util::ShowField($this->needBind, $this->produto->OutrasEspecificacoes); ?></textarea>

                    </div>

                </div>

                <div class="row">

                	<div class="span6">

                       <label for="txtCaminhoImagem">Imagem</label>

                 

                       <?php if (!empty($this->produto->CaminhoImagem)) { ?>

                       <img width="200" height="150" src="..\<?php echo Util::ShowField($this->needBind, $this->produto->CaminhoImagem); ?>" />

                       <?php } ?>

                        <input type="file" name="txtCaminhoImagem" id="txtCaminhoImagem"  class="span6">

                    </div>

                	<div class="span6">

                       <label for="txtCaminhoImagem">Imagem 2</label>

                 

                       <?php if (!empty($this->produto->CaminhoImagem2)) { ?>

                       <img width="200" height="150" src="..\<?php echo Util::ShowField($this->needBind, $this->produto->CaminhoImagem2); ?>" />

                       <?php } ?>

                        <input type="file" name="txtCaminhoImagem2" id="txtCaminhoImagem2"  class="span6">

                    </div>

                </div>
                
                       <div class="row">

                	<div class="span6">

                       <label for="txtCaminhoImagem3">Imagem 3</label>

                 

                       <?php if (!empty($this->produto->CaminhoImagem3)) { ?>

                       <img width="200" height="150" src="..\<?php echo Util::ShowField($this->needBind, $this->produto->CaminhoImagem3); ?>" />

                       <?php } ?>

                        <input type="file" name="txtCaminhoImagem3" id="txtCaminhoImagem3"  class="span6">

                    </div>

                	<div class="span6">

                       <label for="txtCaminhoImagem4">Imagem 4</label>

                 

                       <?php if (!empty($this->produto->CaminhoImagem4)) { ?>

                       <img width="200" height="150" src="..\<?php echo Util::ShowField($this->needBind, $this->produto->CaminhoImagem4); ?>" />

                       <?php } ?>

                        <input type="file" name="txtCaminhoImagem4" id="txtCaminhoImagem4"  class="span6">

                    </div>

                </div>
                <div class="row">
                    
                          <div class="span6">

                    	  <label for="cboCategoria">Categoria</label>

                    	  <select name="cboCategoria" id="cboCategoria" class="span6">



                    	  	<?php foreach (CategoriaProdutoModel::ListarCategorias() as $item)

							{  ?>

                                <option value="<?php echo $item->Id ?>" <?php echo Util::SelectItem($this->needBind, $this->produto->IdCategoria, $item->Id); ?>><?php echo $item->Nome ?></option>

                            <?php } ?>

                            </select>

                    </div>
                </div>

                  <div class="row">

                	<div class="span12 pull-right">

                		 <input type="submit" value="Salvar" class="btn btn-success" />

                		 <input type="button" id="btnVoltar" value="Voltar" class="btn btn-danger" />

                	</div>

                </div>

            </fieldset>

        </form>

    </div>

    <script  type="text/javascript" charset="utf-8">

    	$(document).ready(function() {

    		 $("#btnVoltar").on("click", function(e) {

		 		  window.location.href = "<?php echo dirname($_SERVER['PHP_SELF']); ?>/produtos.php";



    		 });

    	});

    </script>



		<?php 

	}

}







   

  

  

 ?>