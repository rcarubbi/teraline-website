<?php 

class MenuController {

	function RenderMenuProdutos () {
	 	 $htmlResult = "";

	 
	 	 $listaCategorias = CategoriaProdutoModel::ListarCategorias();
	 	 
	     foreach ($listaCategorias as $item) {
	     	  $root = $_SERVER['REMOTE_HOST'];
	     	  $nomeCategoriaURL = str_replace(" ", "-", $item->Nome);
	 	 	  $htmlResult .= "<li><a href=\"$root/produtos/$nomeCategoriaURL/$item->Id\">$item->Nome</a></li>";

	 	 }
		 
	 	 return $htmlResult;

	}

	function RenderFloatMenu()
	{ ?>
	  <div class="navbar-wrapper">
      <div class="container">
        <div class="navbar">
          <div class="navbar-inner">
            <div class="container">
            <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
              <a class="brand" href="<?php echo $_SERVER['REMOTE_HOST'] ?>/home">Teraline Móveis</a>
              <div class="nav-collapse collapse" id="main-menu">
              	<nav>
                <ul class="nav" id="main-menu-left">
                  <li><a href="<?php echo $_SERVER['REMOTE_HOST'] ?>/home">Home</a></li>
                  <li><a href="<?php echo $_SERVER['REMOTE_HOST'] ?>/institucional">A Empresa</a></li>
                  <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Produtos <b class="caret"></b></a>
                    <ul class="dropdown-menu" id="swatch-menu">
                    <?php
                       
                      echo $this->RenderMenuProdutos();
                    ?>
                    </ul>
                  </li>
                  <li><a href="<?php echo $_SERVER['REMOTE_HOST'] ?>/contato">Contato</a></li>
                </ul>
                </nav> 
              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
    }

    function RenderTopMenu()
    {
		?>
		
			<div class="navbar navbar-fixed-top">
				<div class="navbar-inner">
					<div class="container">
					 <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
		              <span class="icon-bar"></span>
		              <span class="icon-bar"></span>
		              <span class="icon-bar"></span>
		            </button>
					<a class="brand" href="<?php echo $_SERVER['REMOTE_HOST'] ?>/home">Teraline Móveis</a>
						<div class="nav-collapse collapse" id="main-menu">
							<ul class="nav" id="main-menu-left">
								<li><a href="<?php echo $_SERVER['REMOTE_HOST'] ?>/home">Home</a></li>
								<li><a href="<?php echo $_SERVER['REMOTE_HOST'] ?>/institucional">A Empresa</a></li>
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#">Produtos <b class="caret"></b></a>
									<ul class="dropdown-menu" id="swatch-menu">
									<?php echo $this->RenderMenuProdutos(); ?>
									</ul>
								</li>
								<li><a href="<?php echo $_SERVER['REMOTE_HOST'] ?>/contato">Contato</a></li>
							</ul> 
						</div>  
					</div>
				</div>
			</div>
	  		
    	<?php
    }
	
	function RenderAdminMenu()
	{
		?>

		<div class="navbar navbar-fixed-top">
				<div class="navbar-inner">
					<div class="container">
					 <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
		              <span class="icon-bar"></span>
		              <span class="icon-bar"></span>
		              <span class="icon-bar"></span>
		            </button>
					<a class="brand" href="index.php">Teraline Móveis</a>
						<div class="nav-collapse collapse" id="main-menu">
							<ul class="nav" id="main-menu-left">
								 
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#">Cadastro<b class="caret"></b></a>
									<ul class="dropdown-menu" id="swatch-menu">
									 	<li><a href="destaques.php">Destaques</a></li>
									 	<li><a href="categorias.php">Categorias</a></li>
									 	<li><a href="produtos.php">Produtos</a></li>
									 	<li><a href="usuarios.php">Usuários</a></li>
									</ul>
								</li>
								<li><a href="configuracoes.php">Configurações</a></li>
								
							</ul> 
							<ul id="user-menu" class="nav pull-right">
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#">
										<i class="icon-user icon-white"></i>
										<?php echo $_SESSION["nomeUsuario"] ?><b class="caret"></b></a>
									<ul class="dropdown-menu" id="swatch-menu">
									 	<li><a href="index.php?logout=1"><i class="icon-off icon-white"></i> Logout</a></li>
									</ul>
								</li>
							</ul>
						</div>  
					</div>
				</div>
			</div>

		<?php
	}
}
 
?>