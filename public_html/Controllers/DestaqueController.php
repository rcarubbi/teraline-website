<?php 
class DestaqueController
{
 	function AtivarDesativarDestaque($id)
 	{

 		$destaque = new DestaqueModel();
 		$destaque->GetById($id);

 		if ($destaque->Ativo == 1)
 		{
 			$destaque->Deactivate();
 		}
 		else
 		{
 			$destaque->Activate();
 		}
 	
 	}


	public $destaqueModel;

    public $needBind = 0;
 	
 	function BindDestaque($id)
 	{
 		
 		$this->destaqueModel = new DestaqueModel();
 		$this->destaqueModel->GetById($id);
 		$this->needBind = 1;

 	}

 	function CreateOrEdit($id, $loadObjectHandler)
 	{
 		if (!is_callable($loadObjectHandler)) throw new Exception('Invalid Load object Handler');
 		$this->destaqueModel = new DestaqueModel();
 		if ($id > 0)
 		{
 			$this->destaqueModel->GetById($id);
 		}
 		$params = array($this->destaqueModel);
 		$this->destaqueModel = call_user_func_array($loadObjectHandler,$params);
 		$this->destaqueModel->Save();
 		header("Location: destaques.php"); 

 	}

 	function ShowForm()
	{
		?>
		<div class="container">
			<form name="form" enctype="multipart/form-data" method="post" action="destaqueForm.php?action=Save&id=<?php echo Util::ShowField($this->needBind, $this->destaqueModel->Id); ?>">
              <fieldset>
                <legend>Destaque</legend>
                 <div class="row">
                    <div class="span12">
                	 
                        <label for="txtTitulo">Título</label>
                        <input name="txtTitulo" id="txtTitulo" type="text" class="span12" value='<?php echo Util::ShowField($this->needBind, $this->destaqueModel->Titulo); ?>' placeholder="Título" required>
                    </div>
                     
                </div>
				<div class="row">
                	<div class="span12">
                       <label for="txtTexto">Texto</label>
                        <textarea name="txtTexto" id="txtTexto" class="span12 ckeditor"><?php echo Util::ShowField($this->needBind, $this->destaqueModel->Texto); ?></textarea>
                    </div>
                </div>
                <div class="row">
                	<div class="span6">
                       <label for="cboCor">Cor do texto</label>
                       <select name="cboCor" id="cboCor">
                            <option value="black" <?php echo Util::SelectItem($this->needBind, $this->destaqueModel->Cor, "black"); ?>>Preto</option>
                            <option value="white" <?php echo Util::SelectItem($this->needBind, $this->destaqueModel->Cor, "white"); ?>>Branco</option>
                            <option value="blue" <?php echo Util::SelectItem($this->needBind, $this->destaqueModel->Cor, "blue"); ?>>Azul</option>
                            <option value="red" <?php echo Util::SelectItem($this->needBind, $this->destaqueModel->Cor, "red"); ?>>Vermelho</option>
                            <option value="green" <?php echo Util::SelectItem($this->needBind, $this->destaqueModel->Cor, "green"); ?>>Verde</option>
                            <option value="purple" <?php echo Util::SelectItem($this->needBind, $this->destaqueModel->Cor, "purple"); ?>>Roxo</option>
                            <option value="gray" <?php echo Util::SelectItem($this->needBind, $this->destaqueModel->Cor, "gray"); ?>>Cinza</option>
                            <option value="yellow" <?php echo Util::SelectItem($this->needBind, $this->destaqueModel->Cor, "yellow"); ?>>Amarelo</option>
                        </select>
                    </div>
                	<div class="span6">
                       <label for="txtCaminhoImagem">Imagem</label>
                 
                       <?php if (!empty($this->destaqueModel->CaminhoImagem)) { ?>
                       <img width="200" height="150" src="..\<?php echo Util::ShowField($this->needBind, $this->destaqueModel->CaminhoImagem); ?>" />
                       <?php } ?>
                        <input type="file" name="txtCaminhoImagem" id="txtCaminhoImagem"  class="span6">
                    </div>
                    
                </div>
                <div class="row">
                	<div class="span6">
						<label for="txtTextoBotao">Texto do Botão</label>
                         <input name="txtTextoBotao" id="txtTextoBotao" type="text" class="span6" value='<?php echo Util::ShowField($this->needBind, $this->destaqueModel->TextoLink); ?>' placeholder="Confira" required>
                	</div>
                	<div class="span6">
                		<label for="txtCaminhoBotao">Caminho do Botão</label>
                         <input name="txtCaminhoBotao" id="txtCaminhoBotao" type="text" class="span6" value='<?php echo Util::ShowField($this->needBind, $this->destaqueModel->CaminhoLink); ?>' placeholder="Caminho da página">
                	</div>
                </div>
                  <div class="row">
                	<div class="span12 pull-right">
                		 <input type="submit" value="Salvar" class="btn btn-success" />
                	</div>
                </div>
            </fieldset>
        </form>
    </div>

		<?php 
	}


	function ShowGrid()
	{
		$destaqueList = DestaqueModel::ListarDestaqueAdmin();
		?>
		<table class="table table-striped table-hover">
			<thead>
			<tr>
				<th>Título</th>
				<th>Texto</th>
				<th>Caminho da Imagem</th>
				<th>Texto do Botão</th>
				<th>Caminho do Botão</th>
				<th>Cor do texto</th>
				<th>Ativo</th>
				<th>Editar</th>
				<th>Ativar/Inativar</th>
			</tr>
			</thead>
			<tbody>
		<?php  
		foreach($destaqueList as $item)
		{

			$icon = "icon-pause";

			if ($item->Ativo == 0)
			{
				$icon = "icon-play";
			}

			$activateButton = "<a href='destaques.php?action=activateDeactivate&id=$item->Id' class='btn btn-primary'><i class='icon-play icon-white'></i></a>";

		 	echo"<tr>
				<td>$item->Titulo</td>
				<td>$item->Texto</td>
				<td>$item->CaminhoImagem</td>
				<td>$item->TextoLink</td>
				<td>$item->CaminhoLink</td>
				<td>$item->Cor</td>
				<td>$item->Ativo</td>
				<td><a href='destaqueForm.php?action=edit&id=$item->Id' class='btn btn-primary'><i class='icon-pencil icon-white'></i></a></td>
				<td><a href='destaques.php?action=activateDeactivate&id=$item->Id' class='btn btn-primary'><i class='$icon icon-white'></i></a></td>
				 
			</tr>";
		 
		} ?>
			<tr>
				<td colspan="9">
					<a href='destaqueForm.php?action=new' class='btn btn-primary pull-right'><i class='icon-plus icon-white'></i></a>
				</td>
			</tr>
			</tbody>
		</table>
		<?php 
	}

}

 ?>