<?php 


class LoginController
{
	function Validar($login, $senha)
	{
		$usuario = new UsuarioModel();
		$usuario->Login = $login;
		$usuario->Senha = base64_encode($senha);
		$usuario->GetByLoginAndPassword();

		if ($usuario->Id > 0)
		{
			if (!isset($_SESSION)) session_start();
			$_SESSION['idUsuario'] = $usuario->Id;
    		$_SESSION['nomeUsuario'] = $usuario->Nome;
    		return true;
		}
		else
		{
			return false;
		}

	}
}

 ?>