<?php 

 
$senderAdmin = Util::Get("admin", false);
 
 
if ($senderAdmin)
{
	include_once "../config/Config.php";
}
else
{
	include_once "./config/Config.php";
}
 

class Util
{

	static function SelectItem($needBind, $value, $valueToCompare)
	{
		$retorno = "";
		if ($needBind == 1 && $value == $valueToCompare)
		{
			$retorno = "Selected";
		}
		 
		return $retorno;
	}

	static function ShowField($needBind, $value)
	{
		
		return ($needBind == 1) ? $value : '';
	}


	static function Get($key, $defaultValue)
	{
		if (isset($_GET[$key]))
		{
		 	return $_GET[$key];
		}
		else
			return $defaultValue;
	}

	static function Post($key, $defaultValue)
	{
		if (isset($_POST[$key]))
		{
		 	return $_POST[$key];
		}
		else
		{
			return $defaultValue;
		}
	}


	static function VerificarAcesso()
	{
		// A sessão precisa ser iniciada em cada página diferente
 		if (!isset($_SESSION)) session_start();

		// Verifica se não há a variável da sessão que identifica o usuário
 		if (!isset($_SESSION['idUsuario'])) {
	 		// Destrói a sessão por segurança
	 		session_destroy();
	 
	    	// Redireciona o visitante de volta pro login
	 		header("Location: login.php"); 
	 		exit;
 		}
	}

	static function mysql_real_escape_string($value)
	{
		$configDB = $GLOBALS["configDB"];
		$con = mysql_connect($configDB["dbhost"], $configDB["dbuser"], $configDB["dbpassword"])  or die(mysql_error());
		$returnValue = mysql_real_escape_string($value);

		mysql_close();

		return $returnValue;
	}

	static function GetObject($sqlStatement, $loadObjectHandler, $instance)
	{
		if (!is_callable($loadObjectHandler)) throw new Exception('Invalid Item List Handler');

		$configDB = $GLOBALS["configDB"];

		$con = mysql_connect($configDB["dbhost"], $configDB["dbuser"], $configDB["dbpassword"])  or die(mysql_error());
		mysql_set_charset('utf8',$con);
		mysql_select_db("teraline", $con)  or die(mysql_error());

		
		$result = mysql_query($sqlStatement);

		if (!$result) {
		    $message  = 'Invalid query: ' . mysql_error() . "\n";
		    $message .= 'Whole query: ' . $sqlStatement;
		    die($message);
		}

		$row = mysql_fetch_array($result, MYSQL_BOTH);
		$params = array($row);
		array_push($params, $instance);
		$object = call_user_func_array($loadObjectHandler,$params);
		
		mysql_free_result($result);
		mysql_close();
	}

	static function InsertNew($sqlStatement)
	{
		$configDB = $GLOBALS["configDB"];
		$con = mysql_connect($configDB["dbhost"], $configDB["dbuser"], $configDB["dbpassword"])  or die(mysql_error());

		mysql_set_charset('utf8',$con);
		mysql_query("SET NAMES 'utf8'");
		mysql_query('SET character_set_connection=utf8');
		mysql_query('SET character_set_client=utf8');
		mysql_query('SET character_set_results=utf8');
		mysql_select_db("teraline", $con)  or die(mysql_error());


		$result = mysql_query($sqlStatement) or die('A error occured: ' . mysql_error());

		$new_id = mysql_insert_id();

		mysql_close();

		return $new_id;
	}


	static function SaveFile($uploadDir, $fieldName)
	{

		$uploadDir = $_SERVER['DOCUMENT_ROOT'] . "/" . str_replace("\\", "/", $uploadDir);

		 
		if (!move_uploaded_file($_FILES[$fieldName]['tmp_name'], $uploadDir . $_FILES[$fieldName]['name'])) {
			//throw new Exception("Erro ao gravar arquivo");
		}
	}

	static function DeleteFile($fileName)
	{
		$filePath = $_SERVER['DOCUMENT_ROOT'] . "/" . str_replace("\\", "/", $fileName);
		if (is_file($filePath) == TRUE)
		{
			@unlink($filePat);
		}
		 
	}

	static function UpdateOrDelete($sqlStatement)
	{
		 
		$configDB = $GLOBALS["configDB"];
		$con = mysql_connect($configDB["dbhost"], $configDB["dbuser"], $configDB["dbpassword"])  or die(mysql_error());

		mysql_select_db("teraline", $con)  or die(mysql_error());
		mysql_set_charset('utf8',$con);
		mysql_query("SET NAMES 'utf8'");
		mysql_query('SET character_set_connection=utf8');
		mysql_query('SET character_set_client=utf8');
		mysql_query('SET character_set_results=utf8');
		 
		$result = mysql_query($sqlStatement) or die('A error occured: ' . mysql_error() );

		mysql_close();
	}

	static function GetList($sqlStatement, $loadItemListHandler)
	{
	 
		if (!is_callable($loadItemListHandler)) throw new Exception('Invalid Item List Handler');

		$configDB = $GLOBALS["configDB"];

		$con = mysql_connect($configDB["dbhost"], $configDB["dbuser"], $configDB["dbpassword"])  or die(mysql_error());
		mysql_set_charset('utf8',$con);
		mysql_query("SET NAMES 'utf8'");
		mysql_query('SET character_set_connection=utf8');
		mysql_query('SET character_set_client=utf8');
		mysql_query('SET character_set_results=utf8');
		mysql_select_db("teraline", $con)  or die(mysql_error());

		
		$result = mysql_query($sqlStatement);

		if (!$result) {
		    $message  = 'Invalid query: ' . mysql_error() . "\n";
		    $message .= 'Whole query: ' . $sqlStatement;
		    die($message);
		}


		$list = array();

		while($row = mysql_fetch_array($result, MYSQL_BOTH))
		{
			$params = array($row);
			$object = call_user_func_array($loadItemListHandler,$params);
			array_push($list, $object);
		}

		mysql_free_result($result);
		mysql_close();

		return $list;
	}


	static function GetDictionary($sqlStatement, $keyFieldName, $valueFieldName)
	{
		$configDB = $GLOBALS["configDB"];

		$con = mysql_connect($configDB["dbhost"], $configDB["dbuser"], $configDB["dbpassword"])  or print(mysql_error());
		mysql_set_charset('utf8',$con);
		mysql_select_db("teraline", $con)  or die(mysql_error());

		
		$result = mysql_query($sqlStatement);

		if (!$result) {
		    $message  = 'Invalid query: ' . mysql_error() . "\n";
		    $message .= 'Whole query: ' . $sqlStatement;
		    die($message);
		}


		$dict = array();

		while($row = mysql_fetch_array($result, MYSQL_BOTH))
		{
			 $dict[$row[$keyFieldName]] = $row[$valueFieldName];
		}

		mysql_free_result($result);
		mysql_close();

		return $dict;
	}


 
 }

 ?>