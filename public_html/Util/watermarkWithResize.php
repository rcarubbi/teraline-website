<?php
 
  // loads a png, jpeg or gif image from the given file name
  function imagecreatefromfile($image_path) {
    // retrieve the type of the provided image file
    list($width, $height, $image_type) = getimagesize($image_path);

    // select the appropriate imagecreatefrom* function based on the determined
    // image type
    switch ($image_type)
    {
      case IMAGETYPE_GIF: return imagecreatefromgif($image_path); break;
      case IMAGETYPE_JPEG: return imagecreatefromjpeg($image_path); break;
      case IMAGETYPE_PNG: return imagecreatefrompng($image_path); break;
      default: return ''; break;
    }
  }

  $imagePath = "../" . urldecode($_GET['image']);
  $watermarkPath = "../" . $_GET['watermark'];
 
header('content-type: image/jpeg');

$image = imagecreatefromfile($imagePath);
$imageSize = getimagesize($imagePath);

$watermark = imagecreatefromfile($watermarkPath);

$watermark_o_width = imagesx($watermark);
$watermark_o_height = imagesy($watermark);

$newWatermarkWidth = $imageSize[0];
$newWatermarkHeight = $imageSize[1]/4;

$success = imagecopyresized($image,                 // Destination image
           $watermark,                              // Source image
           $imageSize[0]/2 - $newWatermarkWidth/2,  // Destination X
           $imageSize[1]/3, // Destination Y
           0,                                       // Source X
           0,                                       // Source Y
           $newWatermarkWidth,                      // Destination W
           $newWatermarkHeight,                     // Destination H
           imagesx($watermark),                     // Source W
           imagesy($watermark));                    // Source H

imagejpeg($image);

imagedestroy($image);
imagedestroy($watermark);

?>