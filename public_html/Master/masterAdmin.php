<?php 
	
	$_GET['admin'] = true; 
	include_once "../Util/Util.php";
    include_once "../Controllers/MenuController.php";
    include_once "../Controllers/DestaqueController.php";
    include_once "../Controllers/ConfiguracoesController.php";
    include_once "../Controllers/CategoriaController.php";
    include_once "../Controllers/UsuarioController.php";
    include_once "../Controllers/ProdutoController.php";
    include_once "../Models/DestaqueModel.php";
    include_once "../Models/ConfiguracaoModel.php";
    include_once "../Models/CategoriaProdutoModel.php";
    include_once "../Models/UsuarioModel.php";
    include_once "../Models/ProdutoModel.php";
           
    Util::VerificarAcesso();
    doControllerLogic();
 ?>
<!--[if gt IE 7]>
<!DOCTYPE html> 
<![endif]-->
 <!--[if lt IE 8]>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<![endif]-->
<html lang="pt-BR">
	<head>
	    <meta charset='utf-8'> 
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <!--[if lt IE 9]>
	        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	    <title>TERA LINE - M&oacute;veis de alta qualidade</title>
	    <link rel="stylesheet" href="../css/slate/bootstrap.css">
	    <link rel="stylesheet" href="../css/bootstrap-responsive.min.css"  />
    	<link rel="stylesheet" href="../css/admin.css"  />
 	 	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	    <script src="../js/bootstrap.js" type="text/javascript" charset="utf-8"></script>
	    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
	    <script src="../js/ckeditor.js" type="text/javascript" ></script>
	    <script src="../js/config.js" type="text/javascript" ></script>
	    <script src="../js/styles.js" type="text/javascript" ></script>
	    <script src="../js/lang/pt-br.js" type="text/javascript" ></script>
	</head>
	<body>
	 <header>
	 <?php 
     renderHeaderMenu(); 
     ?>
 	</header>
 	<?php 
     renderMainContent();
     ?>
	<footer id="footer">
		<div class="container">
			<p class="pull-right"><a class="link" href="#">Voltar ao Topo</a></p>
			<p>© 2013 TERA LINE · Todos os direitos Reservados</p>
		</div>
	</footer>
  	</body>
</html>