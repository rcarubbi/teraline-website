<?php
	include "./Master/master.php";  

	function renderTitle()
    {
        echo "TERA LINE - mesas para refeitório industrial, mesa para refeitório industrial, mesa para refeitório, mesas para refeitório,cadeira para refeitório, bancos para vestiário, vestiário,vestiários, mesa, mesas, cadeiras, bancos, refeitório, mdf, fórmica, móveis, mra, refeitorio, banquetas, teraline, monobloco, tubular, bares";
    }


    function loadObjects()
 	{
 		global $menuController;
 		global $homeController;
		$menuController = new MenuController();
		$homeController = new HomeController();	 

 	}
    
   
 	function renderHeaderMenu()
 	{
		global $menuController;
		$menuController->RenderFloatMenu();
 	}
 	
    function renderMainContent()
    { ?>

		<div id="myCarousel" class="carousel slide">
			<div class="carousel-inner">
				   <?php
                      global $homeController;
                      echo $homeController->RenderDestaques();
                    ?>
			</div>
		</div>

		<script>
			!function ($) {
			$(function(){
			   
			   
			  $('#myCarousel').carousel(
			  {
				 interval: 2000,
				 pause: "hover"
			  })

			})
			}(window.jQuery)
		</script>

<?php  } 
 
?>
