<?php
    

    function doControllerLogic()
    {
        $action = Util::Get("action", "");
        $id = Util::Get("id", 0);
        
        if ($action == "activateDeactivate")
        {
             
            $usuarioController = new UsuarioController();
            $usuarioController->AtivarDesativar($id);

        }
    }

 	function renderHeaderMenu()
 	{
		$menuController = new MenuController();
		$menuController->RenderAdminMenu();
 	}
 	
    function renderMainContent()
    { 
       $usuarioController = new UsuarioController();
       $usuarioController->ShowGrid();
    } 

    include_once "../Master/masterAdmin.php"; 
?>
