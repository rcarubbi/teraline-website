<?php
    

    function doControllerLogic()
    {
        $action = Util::Get("action", "");
        $id = Util::Get("id", 0);
        
        if ($action == "activateDeactivate")
        {
             
            $categoriaController = new CategoriaController();
            $categoriaController->AtivarDesativar($id);

        }
    }

 	function renderHeaderMenu()
 	{
		$menuController = new MenuController();
		$menuController->RenderAdminMenu();
 	}
 	
    function renderMainContent()
    { 
       $categoriaController = new CategoriaController();
       $categoriaController->ShowGrid();
    } 

    include_once "../Master/masterAdmin.php"; 
?>
