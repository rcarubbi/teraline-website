﻿<?php 
	$_GET['admin'] = true; 
	include_once "../Util/Util.php";  
	include_once "../Controllers/LoginController.php"; 
	include_once "../Models/UsuarioModel.php";  

	$txtLogin = Util::Post("txtLogin", "");
	$txtSenha = Util::Post("txtSenha", "");
	  
	if (! empty($txtLogin) && ! empty($txtSenha))
	{
		 
		$loginController = new LoginController();
		if ($loginController->Validar($txtLogin, $txtSenha))
		{
			header("Location: home.php"); 
		}
		else
		{
			$error = true;
		}
	}
 ?>

<!--[if gt IE 7]>
<!DOCTYPE html> 
<![endif]-->
 <!--[if lt IE 8]>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<![endif]-->
<html lang="pt-BR">
	<head>
	    <meta charset='utf-8'> 
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <!--[if lt IE 9]>
	        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	    <title>TERA LINE - M&oacute;veis de alta qualidade</title>
	    <link rel="stylesheet" href="../css/slate/bootstrap.css">
	    <link rel="stylesheet" href="../css/bootstrap-responsive.min.css"  />
    	<link rel="stylesheet" href="../css/admin.css"  />
	 	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	    <script src="../js/bootstrap.js" type="text/javascript" charset="utf-8"></script>
	    <script type="text/javascript">
	    	<?php 
	    		if ($error == true)
	    		{
	    	 ?>
	    	 	$(function() {
      				$("#errorMessage").show();
				});
	    	 	
	    	 <?php 
	    	 	}
	    	  ?>
	    </script>
	</head>
	<body>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="brand" href="admin/admin.php">Teraline - Indústria de Móveis</a>
				 
			</div>
		</div>
	</div>
	<div class="container">
			<div class="content">
				<div class="row">
					<div class="login-form">
						<h2>Login</h2>
						<form action="login.php" method="post">
							<fieldset>
								<div class="clearfix">
									<input name="txtLogin" type="text" placeholder="Login" required>
								</div>
								<div class="clearfix">
									<input name="txtSenha" type="password" placeholder="Senha" required>
								</div>
								<button class="btn primary" type="submit">Entrar</button>
								 
							</fieldset>
						</form>

					</div>
				</div>
			</div>
		</div> <!-- /container -->
			<div id="errorMessage" class="alert alert-error alert-block span3" style="margin-left:70px;display:none;">  
			  <a class="close" data-dismiss="alert">×</a>  
			  <h4 class="alert-heading">Erro de acesso</h4>  
			   Usuário ou senha inválido(s) 
			</div> 
</body>
</html>