<?php
    
     

    function doControllerLogic()
    {
        $action = Util::Get("action", "");
        $id = Util::Get("id", 0);
        global $idCategoriaFiltro;
        $idCategoriaFiltro = Util::Post("cboCategoriaFiltro", 0);
        
        if ($action == "activateDeactivate")
        {
             
            $produtoController = new ProdutoController();
            $produtoController->AtivarDesativar($id);

        }
    }

 	function renderHeaderMenu()
 	{
        global $idCategoriaFiltro;
		$menuController = new MenuController();
		$menuController->RenderAdminMenu();
        $produtoController = new ProdutoController();
        $produtoController->RenderCategoriaFilter($idCategoriaFiltro);
 	}
 	
    function renderMainContent()
    { 
        global $idCategoriaFiltro;
       $produtoController = new ProdutoController();
       $produtoController->ShowGrid($idCategoriaFiltro);
    } 

    include_once "../Master/masterAdmin.php"; 
?>
