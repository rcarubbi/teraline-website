<?php
    include_once "../Master/masterAdmin.php"; 
    $produtoController = null;  
    function doControllerLogic()
    {
        $action = Util::Get("action", "");
        $id = Util::Get("id", 0);
        global $produtoController;
        $produtoController = new ProdutoController();

        if ($action == "edit")
        {
            $produtoController->BindProduto($id);
        }
        elseif ($action == "Save")
        {
           $loadObjectHandler = function($object)
            {

			    if ((($_FILES['txtCaminhoImagem']['type'] == 'image/gif') || ($_FILES['txtCaminhoImagem']['type'] == 'image/jpeg') || ($_FILES['txtCaminhoImagem']['type'] == 'image/png')))
				{
					if (isset($object->CaminhoImagem) && !empty($_FILES["txtCaminhoImagem"]["name"]))
					{
						Util::DeleteFile($object->CaminhoImagem);
					}
				}
				
				if ((($_FILES['txtCaminhoImagem2']['type'] == 'image/gif') || ($_FILES['txtCaminhoImagem2']['type'] == 'image/jpeg') || ($_FILES['txtCaminhoImagem2']['type'] == 'image/png')))
				{
					if (isset($object->CaminhoImagem2) && !empty($_FILES["txtCaminhoImagem2"]["name"]))
					{
						Util::DeleteFile($object->CaminhoImagem2);
					}
				}
				
				if ((($_FILES['txtCaminhoImagem3']['type'] == 'image/gif') || ($_FILES['txtCaminhoImagem3']['type'] == 'image/jpeg') || ($_FILES['txtCaminhoImagem3']['type'] == 'image/png')))
				{
					if (isset($object->CaminhoImagem3) && !empty($_FILES["txtCaminhoImagem3"]["name"]))
					{
						Util::DeleteFile($object->CaminhoImagem3);
					}
				}
				
				if ((($_FILES['txtCaminhoImagem4']['type'] == 'image/gif') || ($_FILES['txtCaminhoImagem4']['type'] == 'image/jpeg') || ($_FILES['txtCaminhoImagem4']['type'] == 'image/png')))
				{
					if (isset($object->CaminhoImagem4) && !empty($_FILES["txtCaminhoImagem4"]["name"]))
					{
						Util::DeleteFile($object->CaminhoImagem4);
					}
				}
				
				$object->Nome = $_POST["txtNome"];
				$object->Codigo = $_POST["txtCodigo"];
				$object->Acabamento = $_POST["txtAcabamento"];
				$object->CaminhoImagem = (empty($_FILES["txtCaminhoImagem"]["name"]))? $object->CaminhoImagem : "img\\\\" . $_FILES["txtCaminhoImagem"]["name"];
				$object->CaminhoImagem2 = (empty($_FILES["txtCaminhoImagem2"]["name"]))? $object->CaminhoImagem2 : "img\\\\" . $_FILES["txtCaminhoImagem2"]["name"];
				$object->CaminhoImagem3 = (empty($_FILES["txtCaminhoImagem3"]["name"]))? $object->CaminhoImagem3 : "img\\\\" . $_FILES["txtCaminhoImagem3"]["name"];
				$object->CaminhoImagem4 = (empty($_FILES["txtCaminhoImagem4"]["name"]))? $object->CaminhoImagem4 : "img\\\\" . $_FILES["txtCaminhoImagem4"]["name"];
				$object->Medidas = $_POST["txtMedidas"];
				$object->Peso = $_POST["txtPeso"];
				$object->OutrasEspecificacoes = $_POST["txtOutrasEspecificacoes"];
				$object->IdCategoria = $_POST["cboCategoria"];
  
			    if ((($_FILES['txtCaminhoImagem']['type'] == 'image/gif') || ($_FILES['txtCaminhoImagem']['type'] == 'image/jpeg') || ($_FILES['txtCaminhoImagem']['type'] == 'image/png')))
				{
					Util::SaveFile("img\\", "txtCaminhoImagem");
				}
				
				if ((($_FILES['txtCaminhoImagem2']['type'] == 'image/gif') || ($_FILES['txtCaminhoImagem2']['type'] == 'image/jpeg') || ($_FILES['txtCaminhoImagem2']['type'] == 'image/png')))
				{
					Util::SaveFile("img\\", "txtCaminhoImagem2");
				}

				if ((($_FILES['txtCaminhoImagem3']['type'] == 'image/gif') || ($_FILES['txtCaminhoImagem3']['type'] == 'image/jpeg') || ($_FILES['txtCaminhoImagem3']['type'] == 'image/png')))
				{
					Util::SaveFile("img\\", "txtCaminhoImagem3");
				}

				if ((($_FILES['txtCaminhoImagem4']['type'] == 'image/gif') || ($_FILES['txtCaminhoImagem4']['type'] == 'image/jpeg') || ($_FILES['txtCaminhoImagem4']['type'] == 'image/png')))
				{
					Util::SaveFile("img\\", "txtCaminhoImagem4");
				}
				
				
				return $object;
			};

            $produtoController->CreateOrEdit($id, $loadObjectHandler);
			 
           
        }
    }

 	function renderHeaderMenu()
 	{
		$menuController = new MenuController();
		$menuController->RenderAdminMenu();
 	}
 	
    function renderMainContent()
    { 
       global $produtoController;
       $produtoController->ShowForm();
    } 

  
?>