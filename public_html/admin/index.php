<?php

    include_once "../Master/masterAdmin.php";  
    
    function doControllerLogic()
    {
	    if (Util::Get("logout", 0))
	    {
	    	session_destroy();
	    	header("Location: login.php"); 
	    }
	}


 	function renderHeaderMenu()
 	{
		$menuController = new MenuController();
		$menuController->RenderAdminMenu();
 	}
 	
    function renderMainContent()
    { ?>

		 <div class="mainContent">
		 		<h1>Area administrativa</h1>
		 </div>

<?php  } 
 
?>
