<?php
    include_once "../Master/masterAdmin.php"; 
    $destaqueController = null;  
    function doControllerLogic()
    {
        $action = Util::Get("action", "");
        $id = Util::Get("id", 0);
        global $destaqueController;
        $destaqueController = new DestaqueController();

        if ($action == "edit")
        {
            $destaqueController->BindDestaque($id);
        }
        elseif ($action == "Save")
        {
           $loadObjectHandler = function($object)
            {
                if (isset($object->CaminhoImagem) && !empty($_FILES["txtCaminhoImagem"]["name"]))
                {
                    Util::DeleteFile($object->CaminhoImagem);

                }

                $object->Titulo = $_POST["txtTitulo"];
                $object->Texto = $_POST["txtTexto"];
                $object->Cor = $_POST["cboCor"];
                $object->CaminhoImagem = (empty($_FILES["txtCaminhoImagem"]["name"]))? $object->CaminhoImagem : "img\\" . $_FILES["txtCaminhoImagem"]["name"];
                $object->TextoLink = $_POST["txtTextoBotao"];
                $object->CaminhoLink = $_POST["txtCaminhoBotao"];

                Util::SaveFile("img\\", "txtCaminhoImagem");

                return $object;
            };

            $destaqueController->CreateOrEdit($id, $loadObjectHandler);

           
        }
    }

 	function renderHeaderMenu()
 	{
		$menuController = new MenuController();
		$menuController->RenderAdminMenu();
 	}
 	
    function renderMainContent()
    { 
       global $destaqueController;
       $destaqueController->ShowForm();
    } 

  
?>
