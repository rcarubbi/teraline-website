<?php
  include "./Master/master.php";  

  function renderTitle()
    {
        echo "TERA LINE - mesas para refeitório industrial, cadeiras para refeitório e bancos para vestiário";
    }


    function loadObjects()
   {
     global $menuController;
     global $homeController;
    $menuController = new MenuController();
    $homeController = new HomeController();   

   }
    
   
   function renderHeaderMenu()
   {
    global $menuController;
    $menuController->RenderFloatMenu();
   }
   
    function renderMainContent()
    { ?>

    <div id="myCarousel" class="carousel slide">
      <div class="carousel-inner">
           <?php
                      global $homeController;
                      echo $homeController->RenderDestaques();
                    ?>
      </div>
    </div>

    <script>
      !function ($) {
      $(function(){
         
         
        $('#myCarousel').carousel(
        {
         interval: 2000,
         pause: "hover"
        })

      })
      }(window.jQuery)
    </script>

<?php  } 
 
?>
