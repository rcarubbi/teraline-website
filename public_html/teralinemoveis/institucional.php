<?php


  function renderTitle()
    {
        echo "TERA LINE - A empresa - mesas para restaurantes, cadeiras para restaurante e bancos para restaurante";
    }

    include "./Master/master.php";  

  function renderHeaderMenu()
   {
    global $menuController;
    $menuController->RenderTopMenu();
   }

   function loadObjects()
   {
     global $menuController;
     global $institucionalController;
    $menuController = new MenuController();
    $institucionalController = new InstitucionalController();   

   }

    function renderMainContent()
    {
    global $institucionalController;
  
       ?>
 
    <div class="container">
      <ul class="breadcrumb">
         <li><a href="<?php $_SERVER['REMOTE_HOST'] ?>/home">Home</a><span class="divider">/</span></li> 
        <li><a href="#" class="active">A Empresa</a></li>  
      </ul>
      <h3><?php echo $institucionalController->RenderTitulo(); ?></h3>
      <h4><?php echo $institucionalController->RenderSubTitulo(); ?></h4>
      <div class="textoInstitucionalContainer">  
        <?php echo $institucionalController->RenderTexto(); ?>
      </div>    
    </div>

        <?php  
    } 

   
?>
