<?php 

class HomeController {

	function RenderDestaques () {
	 	 $htmlResult = "";

	 
	 	 $listaDestaques = DestaqueModel::ListarDestaques();
	 	 $active = "active";
	     foreach ($listaDestaques as $item) {
	 	 	  $htmlResult .= "<div class=\"item $active\">
					<img src=\"$item->CaminhoImagem\" alt=\"\">
					<div class=\"container\">
						<div class=\"carousel-caption\">
							<h1 style=\"color:$item->Cor\">$item->Titulo</h1>
							<p class=\"lead\" style=\"color:$item->Cor\">$item->Texto</p>
							<a class=\"btn btn-large btn-primary\" href=\"$item->CaminhoLink\">$item->TextoLink</a>
						</div>
					</div>
				</div>";

				if ($active != "") $active = "";


	 	 }
		 
	 	 return $htmlResult;

	}

}
 
?>