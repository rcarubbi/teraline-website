  <?php 

class ConfiguracoesController
{

    public $configuracaoModel;
    public $configs;
    public $needBind = 0;

    function CreateOrEdit($loadObjectHandler)
    {
        if (!is_callable($loadObjectHandler)) throw new Exception('Invalid Load object Handler');
        $this->configuracaoModel = new ConfiguracaoModel();
        $params = array( $this->configs);
        $this->configs = call_user_func_array($loadObjectHandler,$params);
        $this->configuracaoModel->Save($this->configs);
        $this->LoadConfiguracoes();
    }


    function LoadConfiguracoes()
    {
        $this->configuracaoModel = new ConfiguracaoModel();
        $this->configs = $this->configuracaoModel->CarregarConfiguracoes();
        $this->needBind = 1;
    }

    function ShowForm()
    {?>
        <div class="container">
            <form name="form" method="post" action="configuracoes.php?action=Save">
                <fieldset>
                    <legend>Destaque</legend>
                    <div class="row">
                        <div class="span4">
                            <label for="cboTema" class="span4">Tema</label>
                        </div>
                        <div class="span6">
                             <select name="cboTema" id="cboTema" class="span4">
                                <option value="amelia" <?php echo Util::SelectItem($this->needBind, $this->configs["tema"], "amelia"); ?>>Amelia</option>
                                <option value="cerulean" <?php echo Util::SelectItem($this->needBind, $this->configs["tema"], "cerulean"); ?>>Cerulean</option>
                                <option value="cosmo" <?php echo Util::SelectItem($this->needBind, $this->configs["tema"], "cosmo"); ?>>Cosmo</option>
                                <option value="cyborg" <?php echo Util::SelectItem($this->needBind, $this->configs["tema"], "cyborg"); ?>>Cyborg</option>
                                <option value="default" <?php echo Util::SelectItem($this->needBind, $this->configs["tema"], "default"); ?>>Default</option>
                                <option value="readable" <?php echo Util::SelectItem($this->needBind, $this->configs["tema"], "readable"); ?>>Readable</option>
                                <option value="simplex" <?php echo Util::SelectItem($this->needBind, $this->configs["tema"], "simplex"); ?>>Simplex</option>
                                <option value="slate" <?php echo Util::SelectItem($this->needBind, $this->configs["tema"], "slate"); ?>>Slate</option>
                                <option value="spacelab" <?php echo Util::SelectItem($this->needBind, $this->configs["tema"], "spacelab"); ?>>SpaceLab</option>
                                <option value="superhero" <?php echo Util::SelectItem($this->needBind, $this->config["tema"], "superhero"); ?>>Super Hero</option>
                                <option value="spruce" <?php echo Util::SelectItem($this->needBind, $this->configs["tema"], "spruce"); ?>>Spruce</option>
                                <option value="united" <?php echo Util::SelectItem($this->needBind, $this->configs["tema"], "united"); ?>>United</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span4">
                            <label for="cboIconColor" class="span4">cor dos Ícones</label>
                        </div>
                        <div class="span6">
                             <select name="cboIconColor" id="cboIconColor" class="span4">
                                <option value="icon-white" <?php echo Util::SelectItem($this->needBind, $this->configs["iconColor"], "icon-white"); ?>>Branco</option>
                                <option value="" <?php echo Util::SelectItem($this->needBind, $this->configs["iconColor"], ""); ?>>Preto</option>
                            </select>
                        </div>
                    </div>
                     <div class="row">
                        <div class="span4">
                            <label for="txtTituloInstitucional" class="span4">Título Institucional</label>
                        </div>
                        <div class="span6">
                             <input name="txtTituloInstitucional" id="txtTituloInstitucional" type="text" class="span6" value='<?php echo Util::ShowField($this->needBind, $this->configs['tituloInstitucional']); ?>' placeholder="Título" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span4">
                            <label for="txtSubTituloInstitucional" class="span4">Sub-título Institucional</label>
                        </div>
                        <div class="span6">
                             <input name="txtSubTituloInstitucional" id="txtSubTituloInstitucional" type="text" class="span6" value='<?php echo Util::ShowField($this->needBind, $this->configs['subTituloInstitucional']); ?>' placeholder="Sub-Título">
                        </div>
                    </div>
                     <div class="row">
                        
                        <div class="span12">
                            <label for="txtTextoInstitucional" class="span12">Texto Institucional</label>
                            <textarea style="height:400px;" name="txtTextoInstitucional" id="txtTextoInstitucional" class="span12 ckeditor"><?php echo Util::ShowField($this->needBind, $this->configs['textoInstitucional']); ?></textarea>
            
                        </div>
                    </div>
                     <div class="row">
                    <div class="span12 pull-right">
                         <input type="submit" value="Salvar" class="btn btn-success" />
                    </div>
                </div>
                </fieldset>
            </form>
        </div>
<?php }
    }
?>