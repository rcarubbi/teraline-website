<?php 

class InstitucionalController {
  

    private $configuracoes;

	function __construct()
	{
		$this->configuracoes = ConfiguracaoModel::CarregarConfiguracoes();
	}

	function RenderTexto () {
		echo $this->configuracoes["textoInstitucional"];
	}

	function RenderTitulo () {
		echo $this->configuracoes["tituloInstitucional"];
	}

	function RenderSubTitulo () {
		 
		echo $this->configuracoes["subTituloInstitucional"];
	}
}
 ?>



