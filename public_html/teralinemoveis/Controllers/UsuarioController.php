<?php 
class UsuarioController
{
    function AtivarDesativar($id)
    {

        $usuario = new UsuarioModel();
        $usuario->GetById($id);
         
        if ($usuario->Ativo == 1)
        {
            $usuario->Deactivate();
        }
        else
        {
            $usuario->Activate();
        }
    
    }

    public $usuarioModel;
    public $needBind = 0;

    function BindUsuario($id)
    {
        
        $this->usuarioModel = new UsuarioModel();
        $this->usuarioModel->GetById($id);
        $this->needBind = 1;

    }

    function CreateOrEdit($id, $loadObjectHandler)
    {
        if (!is_callable($loadObjectHandler)) throw new Exception('Invalid Load object Handler');
        $this->usuarioModel = new UsuarioModel();
        if ($id > 0)
        {
            $this->usuarioModel->GetById($id);
        }
        $params = array($this->usuarioModel);
        $this->usuarioModel = call_user_func_array($loadObjectHandler,$params);
        $this->usuarioModel->Save();
        header("Location: usuarios.php"); 
    }

    function ShowForm()
    {
        ?>
        <div class="container">
            <form name="form" method="post" action="usuarioForm.php?action=Save&id=<?php echo Util::ShowField($this->needBind, $this->usuarioModel->Id); ?>">
              <fieldset>
                <legend>Usuário</legend>
                <div class="row">
                    <div class="span12">
                     
                        <label for="txtNome">Nome</label>
                        <input name="txtNome" id="txtNome" type="text" class="span12" value='<?php echo Util::ShowField($this->needBind, $this->usuarioModel->Nome); ?>' placeholder="Nome do Usuário" required>
                    </div>
                       
                     
                </div>
                 <div class="row">
                    <div class="span6">
                     
                        <label for="txtLogin">Login</label>
                        <input name="txtLogin" id="txtLogin" type="text" class="span6" value='<?php echo Util::ShowField($this->needBind, $this->usuarioModel->Login); ?>' placeholder="Login" required>
                    </div>
                    <div class="span6">
                        <label for="txtPassword">Senha</label>
                        <input name="txtPassword" id="txtPassword" type="password" class="span6" value='<?php echo Util::ShowField($this->needBind, $this->usuarioModel->Senha); ?>' placeholder="Senha" required>
                    </div>
                     
                </div>
                  <div class="row">
                    <div class="span12 pull-right">
                         <input type="submit" value="Salvar" class="btn btn-success" />
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

        <?php 
    }

    function ShowGrid()
    {
        $usuariosList = UsuarioModel::ListarUsuariosAdmin();
        ?>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Login</th>
                <th>Ativo</th>
            </tr>
            </thead>
            <tbody>
        <?php  
        foreach($usuariosList as $item)
        {

            $icon = "icon-pause";

            if ($item->Ativo == 0)
            {
                $icon = "icon-play";
            }

            $activateButton = "<a href='usuarios.php?action=activateDeactivate&id=$item->Id' class='btn btn-primary'><i class='icon-play icon-white'></i></a>";

            echo"<tr>
                <td>$item->Nome</td>
                <td>$item->Login</td>
                <td>$item->Ativo</td>
                <td><a href='usuarioForm.php?action=edit&id=$item->Id' class='btn btn-primary'><i class='icon-pencil icon-white'></i></a></td>
                <td><a href='usuarios.php?action=activateDeactivate&id=$item->Id' class='btn btn-primary'><i class='$icon icon-white'></i></a></td>
                 
            </tr>";
         
        } ?>
            <tr>
                <td colspan="9">
                    <a href='usuarioForm.php?action=new' class='btn btn-primary pull-right'><i class='icon-plus icon-white'></i></a>
                </td>
            </tr>
            </tbody>
        </table>
        <?php 
    }

}

 ?>