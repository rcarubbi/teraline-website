<?php 

class ProdutosController
{
	private $categoria;
	function RenderNomeCategoria()
	{
		echo $this->categoria->Nome;

	}

	function GetTituloPaginaCategoria()
	{
		return $this->categoria->Nome . " - TERALINE Indústria de móveis";
	}



	function RenderProdutos()
	{
		$listaProdutos = ProdutoModel::ListarProdutos($this->categoria->Id);

		echo "<ul class=\"thumbnails\">";
		 
		foreach ($listaProdutos as $item)
		{   
			$root = $_SERVER['REMOTE_HOST'];
			$nomeProdutoURL = urlencode($item->Nome);
			//$caminhoImagem = $root . "\\Util\watermarkWithResize.php?image=" . str_replace("\\", "/", $item->CaminhoImagem) . "&watermark=img/watermark.png";
			$caminhoImagem = "/" . $item->CaminhoImagem;
			
			echo "<li class=\"span6\"><div class=\"media well\">
					<a class=\"pull-left thumbnail\" href=\"$root/produto/$nomeProdutoURL/$item->Id\">
    					<img class=\"media-object img-rounded fotoProduto\"  src=\"$caminhoImagem\">
  					</a>
  					<div class=\"media-body\">
  					 	<h4 class=\"media-heading\">$item->Nome</h4>
  					 	<div class=\"cotainer\">
  					 	<div class=\"row\">
  					 		<div class=\"span3\">
						 		$item->Codigo - Acabamento em $item->Acabamento 
						 	</div> 
						 	<div class=\"span3\">
								<a class=\"btn btn-inverse pull-right\" href=\"$root/produto/$nomeProdutoURL/$item->Id\">Detalhes</a>
						 	</div>
						 </div>
						 </div>
					</div>
				</div></li>";
		}

		echo "</ul>";
		 
	}


	function __construct($idCategoria)
	{
		$this->categoria = new CategoriaProdutoModel();
		$this->categoria->GetById($idCategoria);

	}
}



   
  
  
 ?>