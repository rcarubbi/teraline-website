<?php
    include "./Master/master.php";  

    function loadObjects()
    {
    	global $produtosController;
	    $idCategoria = $_GET["idCategoria"];
		$idCategoria = Util::mysql_real_escape_string($idCategoria);
		$produtosController = new ProdutosController($idCategoria);	 
	}

	function renderHeaderMenu()
 	{
		$menuController = new MenuController();
		$menuController->RenderTopMenu();
 	}

 	function renderTitle()
 	{
 		global $produtosController;
		echo $produtosController->GetTituloPaginaCategoria();
 	}

    function renderMainContent()
    {
    	
		global $produtosController;
		 
     	?>
 
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="<?php $_SERVER['REMOTE_HOST'] ?>/home">Home</a><span class="divider">/</span></li> 
				<li><a href="#">Produtos</a><span class="divider">/</span></li>	
				<li><a href="#" class="active"><?php echo $produtosController->RenderNomeCategoria();  ?></a></li>
			</ul>
			<h3><?php echo $produtosController->RenderNomeCategoria();  ?></h3>
			 
			<div class="container">	
				<?php echo $produtosController->RenderProdutos(); ?>
			</div>		
		</div>

        <?php  
    } 

   
?>
