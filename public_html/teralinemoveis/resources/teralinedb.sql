﻿-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 
-- Versão do Servidor: 5.5.24-log
-- Versão do PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `teraline`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) NOT NULL,
  `Ativo` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`Id`, `Nome`, `Ativo`) VALUES
(1, 'Mesas para Refeitório', b'1'),
(2, 'Banquetas', b'1'),
(3, 'Cadeiras', b'1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `configuracao`
--

CREATE TABLE IF NOT EXISTS `configuracao` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `chave` varchar(500) NOT NULL,
  `valor` varchar(5000) NOT NULL,
  `Ativo` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `chave` (`chave`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `configuracao`
--

INSERT INTO `configuracao` (`Id`, `chave`, `valor`, `Ativo`) VALUES
(1, 'textoInstitucional', '<p>A TERA LINE INDÚSTRIA DE MÓVEIS é uma empresa especializada na fabricação de mesas, cadeiras e bancos.  Nossa variada linha de produtos é comercializada através de nossos revendedores, a qual vem atendendo a clientes  dos mais diversos segmentos como: indústrias, hospitais, academias, clubes esportivos, escolas, hotéis, condomínios,  bares, restaurantes e similares.</p> <p>Nossa empresa trabalha com o que há de melhor em matéria prima, juntamente com um processo de produção que envolve  	metalúrgica, marcenaria, tapeçaria, pintura e acabamento.</p> <p>Atuando no mercado desde 2010, com ética e respeito aos seus clientes, sempre se esmerando pela qualidade,  	agilidade e cumprimento de maneira fiel a seus contratos. Com o objetivo de gerar novos negócios e apresentar  	produtos com preços competitivos e compatíveis com o mercado, nossa empresa investiu em seu pessoal técnico e  	administrativo e incorporou ao seu acervo, equipamentos da melhor tecnologia e qualidade. <p>A TERA LINE preocupa-se com o meio ambiente, por isso trabalhamos com toda a madeira certificada pelo IBAMA.  	Toda nossa matéria-prima possui selo de qualidade e responsabilidade ambiental.</p> ', b'1'),
(2, 'tituloInstitucional', 'Sobre a empresa', b'1'),
(3, 'subTituloInstitucional', '', b'1'),
(4, 'tema', 'cosmo', b'1'),
(5, 'iconColor', 'icon-white', b'1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `destaque`
--

CREATE TABLE IF NOT EXISTS `destaque` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(50) NOT NULL,
  `Texto` varchar(1000) NOT NULL,
  `CaminhoLink` varchar(400) NOT NULL,
  `CaminhoImagem` varchar(400) NOT NULL,
  `TextoLink` varchar(25) NOT NULL,
  `Cor` varchar(10) NOT NULL,
  `Ativo` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `destaque`
--

INSERT INTO `destaque` (`Id`, `Titulo`, `Texto`, `CaminhoLink`, `CaminhoImagem`, `TextoLink`, `Cor`, `Ativo`) VALUES
(1, 'Promoção Imperdível!', 'Cadeira universitária escamoteável com assento e encosto revestidos. Poucas Unidades.', 'produto.php?idProduto=1', 'img\\destaque1.jpg', 'Confira', 'white', b'1'),
(2, 'Mesas para refeitório', 'Mesas em diversos tamanhos para refeitórios industriais.', 'produtos.php?idCategoria=1', 'img\\destaque2.jpg', 'Ver Catálogo', 'black', b'1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE IF NOT EXISTS `produto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) NOT NULL,
  `Codigo` varchar(50) NOT NULL,
  `Acabamento` varchar(100) NOT NULL,
  `Medidas` varchar(1000) NOT NULL,
  `Peso` varchar(5000) NOT NULL,
  `OutrasEspecificacoes` varchar(5000) NOT NULL,
  `CaminhoImagem` varchar(500) NOT NULL,
  `IdCategoria` int(11) NOT NULL,
  `Ativo` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`Id`, `Nome`, `Codigo`, `Acabamento`, `Medidas`, `Peso`, `OutrasEspecificacoes`, `CaminhoImagem`, `IdCategoria`, `Ativo`) VALUES
(1, 'Mesa para refeitório', 'MRA 700', 'Fórmica', '<table class="table table-bordered table-striped table-hover"><thead><tr><th>Medidas dos tampos</th><th>Lugares</th></tr></thead><tbody><tr><td>1,20 x 0,80 m</td><td>4</td></tr><tr><td>1,80 x 0,80 m </td><td>6</td></tr><tr><td>2,40 x 0,80 m</td><td>8</td></tr><tr><td>3,00 x 0,80 m</td><td>10</td></tr></tbody></table>', '<div class="container"> <table  class="table table-bordered table-striped table-hover"><thead><tr><th>Medidas do tampo</th><th>Peso teórico</th></tr></thead><tbody><tr><td>1,20 x 0,80 m</td><td>10,560kg</td></tr><tr><td> 1,80 x 0,80 m</td><td>15,840kg</td></tr><tr><td>2,40 x 0,80 m</td><td> 21,450kg</td></tr><tr><td>3,00 x 0,80 m</td><td>26,400kg</td></tr></tbody></table> </div> <div class="container">  <table  class="table table-bordered table-striped table-hover"><thead><tr><th>Medidas do tampo</th><th>Estrutura monobloco 1 unidade</th></tr></thead><tbody><tr><td>2,40 x 0,80 m</td><td>11,000kg</td></tr></tbody></table> </div>', '<ul><li>Altura do tampo: 0,75 m</li><li><p>Tampo da mesa produzido em MDF com 15mm reengrossado com mais 15mm, totalizando 30mm de espessura em sua borda, laminado em Formica. Acabamento da borda da mesa em fita de PVC - 30x4mm de espessura</p></li><li>Estrutura tipo monobloco em tubo de aço carbono 40x30mm com 1,20mm de parede. (4/6 pés)</li></ul>', 'img\\mra700.jpg', 1, b'1'),
(3, 'Banco para refeitório', 'BRA 800', 'Fórmica', '<table class="table table-bordered table-striped table-hover"><thead><tr><th>Medidas dos assentos</th><th>Lugares</th></tr></thead><tbody><tr><td>1,20 x 0,40 m</td><td>4</td></tr><tr><td>1,80 x 0,40 m</td><td>6</td></tr><tr><td>2,40 x 0,40 m</td><td>8</td></tr><tr><td>3,00 x 0,40 m</td><td>10</td></tr></tbody></table>', '<div class="container"> <table class="table table-bordered table-striped table-hover"><thead><tr><th>Medidas do assento</th><th>Peso teórico</th></tr></thead><tbody><tr><td>1,20 x 0,40 m</td><td>5,280kg</td></tr><tr><td>1,80 x 0,40 m</td><td>7,920kg</td></tr><tr><td>2,40 x 0,40 m</td><td>10,725kg</td></tr><tr><td>3,00 x 0,40 m</td><td>13,200kg</td></tr></tbody></table> </div> <div class="container"> <table class="table table-bordered table-striped table-hover"><thead><tr><th>Medidas do tampo</th><th>Estrutura monobloco 1 unidade</th></tr></thead><tbody><tr><td>p/ 2,40 x 0,40 m</td><td>9,000kg</td></tr></tbody></table> </div>  ', '<ul><li>Altura do assento: 0,45 m</li><li><p>Assento do banco produzido em MDF com 15mm reengrossado com mais 15mm, totalizando 30mm de espessura em sua borda, laminado em Formica. Acabamento da borda do banco em fita de PVC - 30x4mm de espessura</p></li><li>Estrutura tipo monobloco em tubo de aço carbono 30x30mm com 1,20mm de parede. (4/6 pés)</li></ul>', 'img\\BRA800.jpg', 1, b'1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `login` varchar(30) NOT NULL,
  `senha` varchar(500) NOT NULL,
  `ativo` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `login`, `senha`, `ativo`) VALUES
(1, 'Raphael', 'rcarubbi', 'cmFwaGFrZjA2MTI=', b'1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
