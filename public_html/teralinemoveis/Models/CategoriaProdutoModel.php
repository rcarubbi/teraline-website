<?php



class CategoriaProdutoModel 
{
	public $Id;
	public $Nome;

	static function ListarCategorias() {
	

		$loadCategorias = function($row){
			$categoria = new CategoriaProdutoModel();
			$categoria->Id = $row["Id"];
			$categoria->Nome = $row["Nome"];
			return $categoria;

		};

		 

	 	$listaCategorias = Util::GetList("SELECT Id, Nome from categoria where Ativo = (1)", $loadCategorias);

		
		return $listaCategorias;
	}

	static function ListarCategoriasAdmin()
	{
			$loadCategoria = function($row){
			$categoria = new CategoriaProdutoModel();
			$destaque->Id = $row["Id"];
			$destaque->Nome = $row["Nome"];
			$destaque->Ativo =  $row["Ativo"];
			return $destaque;
		};


	 	$listaDestaques = Util::GetList("SELECT Id, Nome, Ativo from categoria", $loadCategoria);


		return $listaDestaques;

	}


	function GetById($idCategoria)
	{
		$loadCategoria = function($row, $instance){
			$instance->Id = $row["Id"];
			$instance->Nome = $row["Nome"];
			$instance->Ativo = $row["Ativo"];
		};

	    Util::GetObject("Select Id, Nome, Ativo from categoria where id = $idCategoria", $loadCategoria, $this);
	}

	function Save()
	{	
		
		if ($this->Id == 0)
		{
			$this->Id = Util::InsertNew("INSERT INTO categoria
			 	(
			 		 Nome 
			 		 
		 		)
		 		VALUES
		 		(
	 				\"$this->Nome\"
	 			);");
		}
		else
		{
			 
			Util::UpdateOrDelete("UPDATE categoria
			 SET 
				 Nome =  \"$this->Nome\"
			 WHERE 
			 	Id = $this->Id"
			 );
		}

	}

	function Deactivate()
	{	
		Util::UpdateOrDelete("UPDATE categoria set Ativo = 0 where Id = $this->Id");
	}
	
	function Activate()
	{
		Util::UpdateOrDelete("UPDATE categoria set Ativo = 1 where Id = $this->Id");
	}

}

?>
 