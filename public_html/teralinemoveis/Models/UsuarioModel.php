<?php 

class UsuarioModel
{
	public $Id = 0;
	public $Nome;
	public $Login;
	public $Senha;

	function GetByLoginAndPassword()
	{
		 $loadUsuario = function($row, $instance){
			$instance->Id = $row["id"];
			$instance->Nome = $row["nome"];
			$instance->Login = $row["login"];
			$instance->Senha = $row["senha"];
		};

	    Util::GetObject("SELECT id, nome, login, senha from usuario where Login = '$this->Login' and senha = '$this->Senha' and ativo = (1)", $loadUsuario, $this);
	}

	static function ListarUsuariosAdmin()
	{
			$loadUsuario = function($row){
			$usuario = new UsuarioModel();
			$usuario->Id = $row["id"];
			$usuario->Nome = $row["nome"];
			$usuario->Ativo =  $row["ativo"];
			$usuario->Login =  $row["login"];
			$usuario->Senha =  $row["senha"];
			return $usuario;
		};

	 	$listaUsuarios = Util::GetList("SELECT id, nome, login, senha, ativo from usuario", $loadUsuario);
		return $listaUsuarios;
	}

	function GetById($idUsuario)
	{
		$loadUsuario = function($row, $instance){
			$instance->Id = $row["id"];
			$instance->Nome = $row["nome"];
			$instance->Ativo = $row["ativo"];
			$instance->Login = $row["login"];
			$instance->Senha = $row["senha"];
		};

	    Util::GetObject("Select id, nome, ativo, login, senha from usuario where id = $idUsuario", $loadUsuario, $this);
	}

	function Save()
	{	
		
		if ($this->Id == 0)
		{
			$this->Id = Util::InsertNew("INSERT INTO usuario
			 	(
			 		 nome,
			 		 login,
			 		 senha
		 		)
		 		VALUES
		 		(
	 				\"$this->Nome\"
	 				,\"$this->Login\"
	 				,\"$this->Senha\"
	 			);");
		}
		else
		{
			Util::UpdateOrDelete("UPDATE usuario
			 SET 
				 nome =  \"$this->Nome\"
				 ,login =  \"$this->Login\"
				 ,senha =  \"$this->Senha\"
			 WHERE 
			 	Id = $this->Id"
			 );
		}

	}

	function Deactivate()
	{	
		Util::UpdateOrDelete("UPDATE usuario set Ativo = 0 where Id = $this->Id");
	}
	
	function Activate()
	{
		Util::UpdateOrDelete("UPDATE usuario set Ativo = 1 where Id = $this->Id");
	}
}
 ?>