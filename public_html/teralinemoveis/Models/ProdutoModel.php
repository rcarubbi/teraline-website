	<?php

class ProdutoModel 
{
	public $Id;
	public $Nome;
	public $Codigo;
	public $Acabamento;
	public $Medidas;
	public $Peso;
	public $OutrasEspecificacoes;
	public $CaminhoImagem;
	public $IdCategoria;
	public $Categoria;
	public $Ativo;
	public $Ordem;

	function Deactivate()
	{	
		Util::UpdateOrDelete("UPDATE produto set Ativo = 0 where Id = $this->Id");
	}
	
	function Activate()
	{
		Util::UpdateOrDelete("UPDATE produto set Ativo = 1 where Id = $this->Id");
	}


	static function ListarProdutos($idCategoria) {

		$loadProdutos = function($row){
			$produto = new ProdutoModel();
			$produto->Id = $row["Id"];
			$produto->Nome = $row["Nome"];
			$produto->Codigo = $row["Codigo"];
			$produto->Acabamento = $row["Acabamento"];
			$produto->Medidas = $row["Medidas"];
			$produto->Peso = $row["Peso"];
			$produto->OutrasEspecificacoes = $row["OutrasEspecificacoes"];
			$produto->CaminhoImagem = str_replace("\\", "/", $row["CaminhoImagem"]);
			$produto->IdCategoria = $row["IdCategoria"];
			 
			return $produto;

		};

	 	$listaProdutos = Util::GetList("SELECT Id, Nome, Codigo, Acabamento, Medidas, Peso, OutrasEspecificacoes, CaminhoImagem, IdCategoria from produto where Ativo = (1) and IdCategoria = $idCategoria order by ordem", $loadProdutos);

		
		return $listaProdutos;
	}

	static function ListarProdutosAdmin($idCategoria = 0)
	{
			$loadProdutos = function($row){
			$produto = new ProdutoModel();
			$produto->Id = $row["Id"];
			$produto->Nome = $row["Nome"];
			$produto->Codigo = $row["Codigo"];
			$produto->Acabamento = $row["Acabamento"];
			$produto->Medidas = $row["Medidas"];
			$produto->Peso = $row["Peso"];
			$produto->OutrasEspecificacoes = $row["OutrasEspecificacoes"];
			$produto->CaminhoImagem = str_replace("\\", "/", $row["CaminhoImagem"]);
			$produto->IdCategoria = $row["IdCategoria"];
			$produto->Ativo = $row["Ativo"];
			$produto->Ordem =  $row["Ordem"];
			$produto->Categoria = new CategoriaProdutoModel();
			$produto->Categoria->Id = $produto->IdCategoria;
			$produto->Categoria->Nome =  $row["nomeCategoria"];
			return $produto;

		};

	 	$listaProdutos = Util::GetList("SELECT p.Id,  p.Nome,  p.Codigo,  p.Acabamento,  p.Medidas,  p.Peso,  p.OutrasEspecificacoes,  p.CaminhoImagem,  p.IdCategoria,  p.Ativo, c.Nome as nomeCategoria, p.Ordem from produto p inner join categoria c on p.IdCategoria = c.Id  where c.Ativo = 1 and (c.Id = $idCategoria or $idCategoria = 0) order by c.Nome, p.Ordem", $loadProdutos);
	
		return $listaProdutos;
	}

	function GetById($idProduto)
	{
		$loadProduto = function($row, $instance){
			$instance->Id = $row["Id"];
			$instance->Nome = $row["Nome"];
			$instance->Codigo = $row["Codigo"];
			$instance->Acabamento = $row["Acabamento"];
			$instance->Medidas = $row["Medidas"];
			$instance->Peso = $row["Peso"];
			$instance->OutrasEspecificacoes = $row["OutrasEspecificacoes"];
			$instance->CaminhoImagem = str_replace("\\", "/", $row["CaminhoImagem"]);
			$instance->IdCategoria = $row["IdCategoria"];
			$instance->Ativo = $row["Ativo"];
			$instance->Ordem = $row["Ordem"];

		};

	    Util::GetObject("SELECT Id, Nome, Codigo, Acabamento, Medidas, Peso, OutrasEspecificacoes, CaminhoImagem, IdCategoria, Ativo, Ordem from produto where Id = $idProduto", $loadProduto, $this);
	}

	function Save()
	{	
		$this->CaminhoImagem = str_replace("\\", "\\\\" , $this->CaminhoImagem);
		if ($this->Id == 0)
		{
 			
			$this->Id = Util::InsertNew("INSERT INTO produto
			 	(
			 		 Nome 
			 		,Codigo
	 				,Acabamento
			 		,Medidas
			 		,Peso
			 		,OutrasEspecificacoes
		 			,CaminhoImagem  
		 			,IdCategoria
		 		)
		 		VALUES
		 		(
	 				\"$this->Nome\"
 					,\"$this->Codigo\"
 					,\"$this->Acabamento\"
 					,\"$this->Medidas\"
 					,\"$this->Peso\"
		 			,\"$this->OutrasEspecificacoes\"
		 			,\"$this->CaminhoImagem\"
		 			,\"$this->IdCategoria\"
	 			);");
		}
		else
		{
			Util::UpdateOrDelete("UPDATE produto
			 SET 
				 Nome =  \"$this->Nome\"
				,Codigo = \"$this->Codigo\" 
				,Acabamento = \"" . Util::mysql_real_escape_string($this->Acabamento) . "\"
				,Medidas = \"" . Util::mysql_real_escape_string($this->Medidas) . "\"
				,Peso = \"" . Util::mysql_real_escape_string($this->Peso) . "\"
				,OutrasEspecificacoes = \"$this->OutrasEspecificacoes\"
				,CaminhoImagem = \"$this->CaminhoImagem\"
				,IdCategoria = \"$this->IdCategoria\"
				,Ordem = \"$this->Ordem\"
			 WHERE 
			 	Id = $this->Id"
			 );
		}

	}
}

?>
 