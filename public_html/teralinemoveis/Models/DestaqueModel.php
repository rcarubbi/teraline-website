<?php

class DestaqueModel 
{

	public $Id;
	public $Titulo;
	public $Texto;
	public $CaminhoLink;
	public $CaminhoImagem;
	public $TextoLink;
	public $Cor;
	public $Ativo;


	function Save()
	{	
		$this->CaminhoImagem = str_replace("\\", "\\\\" , $this->CaminhoImagem);
		if ($this->Id == 0)
		{
			$this->Id = Util::InsertNew("INSERT INTO destaque
			 	(
			 		 Titulo 
			 		,Texto
			 		,CaminhoLink
		 			,CaminhoImagem  
		 			,TextoLink
		 			,Cor
		 		)
		 		VALUES
		 		(
	 				\"$this->Titulo\"
 					,\"$this->Texto\"
 					,\"$this->CaminhoLink\"
 					,\"$this->CaminhoImagem\"
 					,\"$this->TextoLink\"
		 			,\"$this->Cor\"
	 			);");
		}
		else
		{
			 
			Util::UpdateOrDelete("UPDATE destaque
			 SET 
				 Titulo =  \"$this->Titulo\"
				 ,Texto = \"$this->Texto\"
				 ,CaminhoLink = \"$this->CaminhoLink\"
				 ,CaminhoImagem = \"$this->CaminhoImagem\"
				 ,TextoLink = \"$this->TextoLink\"
				 ,Cor = \"$this->Cor\"
			 WHERE 
			 	Id = $this->Id"
			 );
		}

	}

	 

	function Deactivate()
	{	
		Util::UpdateOrDelete("UPDATE destaque set Ativo = 0 where Id = $this->Id");
	}
	
	function Activate()
	{
		Util::UpdateOrDelete("UPDATE destaque set Ativo = 1 where Id = $this->Id");
	}

	function GetById($id)
	{
		$loadDestaque = function($row, $instance){
			$instance->Id = $row["Id"];
			$instance->Titulo = $row["Titulo"];
			$instance->Texto = $row["Texto"];
			$instance->CaminhoLink = str_replace("\\", "/", $row["CaminhoLink"]);
			$instance->TextoLink = $row["TextoLink"];
			$instance->CaminhoImagem = str_replace("\\", "/", $row["CaminhoImagem"]);
			$instance->Cor = $row["Cor"];
			$instance->Ativo = $row["Ativo"];
		};

	    Util::GetObject("SELECT Id, Titulo, Texto, CaminhoLink, CaminhoImagem, TextoLink, Cor, Ativo from destaque where Id = $id", $loadDestaque, $this);

	     
	}

	static function ListarDestaques() {
		
		$loadDestaque = function($row){
			$destaque = new DestaqueModel();
			$destaque->Id = $row["Id"];
			$destaque->Titulo = $row["Titulo"];
			$destaque->Texto = $row["Texto"];
			$destaque->CaminhoLink = str_replace("\\", "/", $row["CaminhoLink"]);
			$destaque->TextoLink = $row["TextoLink"];
			$destaque->CaminhoImagem = str_replace("\\", "/", $row["CaminhoImagem"]);
			$destaque->Cor = $row["Cor"];
			return $destaque;
		};


	 	$listaDestaques = Util::GetList("SELECT Id, Titulo, Texto, CaminhoLink, CaminhoImagem, TextoLink, Cor from destaque where Ativo = (1)", $loadDestaque);


		return $listaDestaques;
	}

	static function ListarDestaqueAdmin()
	{
			$loadDestaque = function($row){
			$destaque = new DestaqueModel();
			$destaque->Id = $row["Id"];
			$destaque->Titulo = $row["Titulo"];
			$destaque->Texto = $row["Texto"];
			$destaque->CaminhoLink = str_replace("\\", "/", $row["CaminhoLink"]);
			$destaque->TextoLink = $row["TextoLink"];
			$destaque->CaminhoImagem = str_replace("\\", "/", $row["CaminhoImagem"]);
			$destaque->Cor = $row["Cor"];
			$destaque->Ativo =  $row["Ativo"];
			return $destaque;
		};


	 	$listaDestaques = Util::GetList("SELECT Id, Titulo, Texto, CaminhoLink, CaminhoImagem, TextoLink, Cor, Ativo from destaque", $loadDestaque);


		return $listaDestaques;

	}

}

?>