<?php
    include_once "../Master/masterAdmin.php"; 
    $usuarioController = null;  
    function doControllerLogic()
    {
        $action = Util::Get("action", "");
        $id = Util::Get("id", 0);
        global $usuarioController;
        $usuarioController = new UsuarioController();

        if ($action == "edit")
        {
            $usuarioController->BindUsuario($id);
        }
        elseif ($action == "Save")
        {
           $loadObjectHandler = function($object)
            {
                 
                $object->Nome = $_POST["txtNome"];
                $object->Login = $_POST["txtLogin"];
                $object->Senha = base64_encode($_POST["txtPassword"]);
                return $object;
            };

            $usuarioController->CreateOrEdit($id, $loadObjectHandler);
        }
    }

 	function renderHeaderMenu()
 	{
		$menuController = new MenuController();
		$menuController->RenderAdminMenu();
 	}
 	
    function renderMainContent()
    { 
       global $usuarioController;
       $usuarioController->ShowForm();
    } 

  
?>
