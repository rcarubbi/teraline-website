<?php
    

    function doControllerLogic()
    {
        $action = Util::Get("action", "");
        $id = Util::Get("id", 0);
        
        if ($action == "activateDeactivate")
        {
             
            $destaqueController = new DestaqueController();
            $destaqueController->AtivarDesativarDestaque($id);

        }
    }

 	function renderHeaderMenu()
 	{
		$menuController = new MenuController();
		$menuController->RenderAdminMenu();
 	}
 	
    function renderMainContent()
    { 
       $destaqueController = new DestaqueController();
       $destaqueController->ShowGrid();
    } 

    include_once "../Master/masterAdmin.php"; 
?>
