<?php
    include_once "../Master/masterAdmin.php"; 
    $categoriaController = null;  
    function doControllerLogic()
    {
        $action = Util::Get("action", "");
        $id = Util::Get("id", 0);
        global $categoriaController;
        $categoriaController = new CategoriaController();

        if ($action == "edit")
        {
            $categoriaController->BindCategoria($id);
        }
        elseif ($action == "Save")
        {
           $loadObjectHandler = function($object)
            {
                 
                $object->Nome = $_POST["txtNome"];
                return $object;
            };

            $categoriaController->CreateOrEdit($id, $loadObjectHandler);
        }
    }

 	function renderHeaderMenu()
 	{
		$menuController = new MenuController();
		$menuController->RenderAdminMenu();
 	}
 	
    function renderMainContent()
    { 
       global $categoriaController;
       $categoriaController->ShowForm();
    } 

  
?>
