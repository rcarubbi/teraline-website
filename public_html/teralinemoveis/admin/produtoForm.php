<?php
    include_once "../Master/masterAdmin.php"; 
    $produtoController = null;  
    function doControllerLogic()
    {
        $action = Util::Get("action", "");
        $id = Util::Get("id", 0);
        global $produtoController;
        $produtoController = new ProdutoController();

        if ($action == "edit")
        {
            $produtoController->BindProduto($id);
        }
        elseif ($action == "Save")
        {
           $loadObjectHandler = function($object)
            {
                if (isset($object->CaminhoImagem) && !empty($_FILES["txtCaminhoImagem"]["name"]))
                {
                    Util::DeleteFile($object->CaminhoImagem);
                }
 
                $object->Nome = $_POST["txtNome"];
                $object->Codigo = $_POST["txtCodigo"];
                $object->Acabamento = $_POST["txtAcabamento"];
                $object->CaminhoImagem = (empty($_FILES["txtCaminhoImagem"]["name"]))? $object->CaminhoImagem : "img\\" . $_FILES["txtCaminhoImagem"]["name"];
                $object->Medidas = $_POST["txtMedidas"];
                 $object->Peso = $_POST["txtPeso"];
                $object->OutrasEspecificacoes = $_POST["txtOutrasEspecificacoes"];
                $object->IdCategoria = $_POST["cboCategoria"];

                Util::SaveFile("img\\", "txtCaminhoImagem");

                return $object;
            };

            $produtoController->CreateOrEdit($id, $loadObjectHandler);

           
        }
    }

 	function renderHeaderMenu()
 	{
		$menuController = new MenuController();
		$menuController->RenderAdminMenu();
 	}
 	
    function renderMainContent()
    { 
       global $produtoController;
       $produtoController->ShowForm();
    } 

  
?>
