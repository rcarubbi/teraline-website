<?php
    include_once "../Master/masterAdmin.php"; 
    $configuracoesController = null;
    function doControllerLogic()
    {
        $action = Util::Get("action", "");
        global $configuracoesController;
        $configuracoesController = new ConfiguracoesController();

        if ($action == "Save")
        {
           $loadObjectHandler = function($object)
            {
                
                $object["tema"] = $_POST["cboTema"];
                $object["iconColor"] = $_POST["cboIconColor"];
                $object["tituloInstitucional"] = $_POST["txtTituloInstitucional"];
                $object["subTituloInstitucional"] = $_POST["txtSubTituloInstitucional"];
                $object["textoInstitucional"] = $_POST["txtTextoInstitucional"];
                return $object;
            };

            $configuracoesController->CreateOrEdit($loadObjectHandler);
        }
        else
        {

            $configuracoesController->LoadConfiguracoes();
        }

    }

 	function renderHeaderMenu()
 	{
		$menuController = new MenuController();
		$menuController->RenderAdminMenu();
 	}
 	
    function renderMainContent()
    { 
        global $configuracoesController;
        $configuracoesController->ShowForm();
    } 

  
?>
