<?php
header("Content-Type: text/xml;charset=utf-8");
echo '<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>http://www.teralinemoveis.com.br/home</loc> 
      </url>
        <url>
        <loc>http://www.teralinemoveis.com.br/institucional</loc>
       </url>';

include_once "./Util/Util.php";
include_once "./Models/CategoriaProdutoModel.php";
include_once "./Models/ProdutoModel.php";

$categoriaModel = new CategoriaProdutoModel();
$listaCategorias = $categoriaModel->ListarCategorias();
foreach ($listaCategorias as $item)
{  

  $nomeAmigavelCategoria = str_replace(" ", "-", $item->Nome);
  echo '<url>
        <loc>http://www.teralinemoveis.com.br/produtos/' . $nomeAmigavelCategoria . '/' . $item->Id . '/</loc>
        <changefreq>weekly</changefreq>
          </url>'; 

    $produtoModel =  new ProdutoModel();
  $listaProdutos =  $produtoModel->ListarProdutos($item->Id);
  foreach ($listaProdutos as $item)
  {  
    $nomeAmigavelProduto = str_replace(" ", "-", $item->Nome);
    echo '<url>
    <loc>http://www.teralinemoveis.com.br/produto/' . $nomeAmigavelProduto. '/' . $item->Id . '/</loc>
    <changefreq>weekly</changefreq>
    </url>'; 
  }
}

echo '<url>
        <loc>http://www.teralinemoveis.com.br/contato</loc>
       </url>';

echo '</urlset>';