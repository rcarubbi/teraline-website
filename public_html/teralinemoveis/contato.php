<?php

    include "./Master/master.php";  

    function renderTitle()
    {
        echo "TERA LINE - entre em Contato - mesas para restaurante, cadeiras para restaurante e bancos para restaurante";
    }

    function loadObjects()
    {
        global  $configuracoes;
        global  $menuController;
       $configuracoes = ConfiguracaoModel::CarregarConfiguracoes();
       $menuController = new MenuController();
    }

    function renderMainContent()
    {
        global  $configuracoes;    
        ?>
        <div class="container">
           
            <ul class="breadcrumb">
                <li><a href="<?php $_SERVER['REMOTE_HOST'] ?>\home">Home</a><span class="divider">/</span></li> 
                <li><a href="#" class="active">Contato</a></li>   
            </ul>
            <form name="form" method="post" action="http://www18.locaweb.com.br/scripts/formmail.pl">
              <input name="recipient" type="hidden" value="vendas@teraline.com.br">
              <input name="subject" type="hidden" value="Formulário de Contato do Site">
              <input name="redirect" type="hidden" value="http://www.teraline.com.br/index.php">
              <input type="hidden" name="email" value="vendas@teraline.com.br">
              <fieldset>
                <legend>Solicite seu orçamento ou envie suas sugestõs através do formulário abaixo.</legend>
               
                 <div class="row">
                    <div class="span6">

                        <label for="txtNome"><i class="icon-user <?php echo $configuracoes["iconColor"] ?>"></i>Nome</label>
                        <input name="txtNome" id="txtNome" type="text" class="span6" placeholder="Nome do contato" required>
                    </div>
                     <div class="span6">
                       <label for="txtEmpresa"><i class="icon-briefcase <?php echo $configuracoes["iconColor"] ?>"></i>Empresa</label>
                        <input name="txtEmpresa" id="txtEmpresa"  type="text"  class="span6" placeholder="Nome da empresa" required>
                    </div>
                </div>
                 <div class="row">
                    <div class="span6">
                          <label for="txtTelefone"><i class="icon-th <?php echo $configuracoes["iconColor"] ?>"></i>Telefone</label>
                          <input name="txtTelefone" id="txtTelefone" type="text" placeholder="(11) 1111-1111" required>
                    </div>
                     <div class="span6">
                        <label for="txtRamal"><i class="icon-th-large <?php echo $configuracoes["iconColor"] ?>"></i>Ramal</label>
                        <input name="txtRamal" id="txtRamal" type="text" class="span1" placeholder="11">
                    </div>
                </div>
                 <div class="row">
                    <div class="span6">
                            <label for="txtCelular"><i class="icon-signal  <?php echo $configuracoes["iconColor"] ?>"></i> Celular</label>
                            <input name="txtCelular" id="txtCelular" type="text" placeholder="(11) 11111-1111">
                    </div>
                     <div class="span6">
                        <label for="txtEmail"><i class="icon-envelope <?php echo $configuracoes["iconColor"] ?>"></i>E-mail</label>
                        <input name="replyto" id="txtEmail" type="text" class="span5" placeholder="contato@site.com" required>
                    </div>
                </div>
                <div class="row">
                    <div class="span6">
                           <label for="txtEndereco"><i class="icon-home <?php echo $configuracoes["iconColor"] ?>"></i>Endereço</label>
                            <input  name="txtEndereco" id="txtEndereco" type="text" class="span6" placeholder="Logradouro, número e complemento">
                    </div>
                     <div class="span6">
                         <label for="txtCidade"><i class="icon-globe <?php echo $configuracoes["iconColor"] ?>"></i>Cidade</label>
                        <input  name="txtCidade" id="txtCidade" type="text" class="span4" placeholder="Nome da cidade" >
                    </div>
                </div>
                <div class="row">
                    <div class="span6">
                        <label for="cboEstado"><i class="icon-globe <?php echo $configuracoes["iconColor"] ?>"></i>Estado</label>
                        <select name="cboEstado" id="cboEstado">
                            <option value="">-- Selecione a UF --</option>
                            <option value="AC">Acre - AC</option>
                            <option value="AL">Alagoas - AL</option>
                            <option value="AP">Amapá - AP</option>
                            <option value="AM">Amazonas - AM</option>
                            <option value="BA">Bahia  - BA</option>
                            <option value="CE">Ceará - CE</option>
                            <option value="DF">Distrito Federal  - DF</option>
                            <option value="ES">Espírito Santo - ES</option>
                            <option value="GO">Goiás - GO</option>
                            <option value="MA">Maranhão - MA</option>
                            <option value="MT">Mato Grosso - MT</option>
                            <option value="MS">Mato Grosso do Sul - MS</option>
                            <option value="MG">Minas Gerais - MG</option>
                            <option value="PA">Pará - PA</option>
                            <option value="PB">Paraíba - PB</option>
                            <option value="PR">Paraná - PR</option>
                            <option value="PE">Pernambuco - PE</option>
                            <option value="PI">Piauí - PI</option>
                            <option value="RJ">Rio de Janeiro - RJ</option>
                            <option value="RN">Rio Grande do Norte - RN</option>
                            <option value="RS">Rio Grande do Sul - RS</option>
                            <option value="RO">Rondônia - RO</option>
                            <option value="RR">Roraima - RR</option>
                            <option value="SC">Santa Catarina - SC</option>
                            <option value="SP">São Paulo - SP</option>
                            <option value="SE">Sergipe - SE</option>
                            <option value="TO">Tocantins - TO</option>
                        </select>
                    </div>
                     <div class="span6">
                        <label for="txtCep"><i class="icon-inbox <?php echo $configuracoes["iconColor"] ?>"></i>CEP</label>
                        <input name="txtCep" id="txtCep" type="text" class="span2" placeholder="00000-000" >
                    </div>
                </div>
                 <div class="row">
                    <div class="span11" >
                        <label for="txtComentarios"> <i class="icon-align-justify <?php echo $configuracoes["iconColor"] ?>"></i>Comentários</label>
                        <textarea class="span12" style="height:100px" id="txtComentarios" name="txtComentarios"> </textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="span12" >
                        <button type="submit" class="btn btn-primary pull-right"><i class="icon-ok <?php echo $configuracoes["iconColor"] ?>"></i>Enviar</button>
                    </div>
                </div>

              </fieldset>
               <span class="help-block">Se preferir, envie correspondência para: <address>Rua Bartolomeu Paes, 579 - São Paulo - SP - CEP 05092-000.</address></span>
               <span class="help-block">A Tera Line aguarda seu contato.</span>
               <div class="container well span4 offset3" >
                   <iframe width="370" height="370" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=R.+Bartolomeu+Pais,+579+-+Vila+Anast%C3%A1cio,+Lapa,+S%C3%A3o+Paulo,+05092-000,+Brasil&amp;daddr=&amp;hl=pt&amp;geocode=CS7TfBkyc-AJFdczmf4dEiA3_SkR7i8g7vjOlDH3LKXQ_harhw&amp;sll=-23.514158,-46.718953&amp;sspn=0.010684,0.021136&amp;g=Rua+Bartolomeu+Paes,+579+-+S%C3%A3o+Paulo+-+SP&amp;mra=mift&amp;ie=UTF8&amp;t=m&amp;ll=-23.514177,-46.718974&amp;spn=0.011806,0.012875&amp;z=15&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=d&amp;source=embed&amp;saddr=R.+Bartolomeu+Pais,+579+-+Vila+Anast%C3%A1cio,+Lapa,+S%C3%A3o+Paulo,+05092-000,+Brasil&amp;daddr=&amp;hl=pt&amp;geocode=CS7TfBkyc-AJFdczmf4dEiA3_SkR7i8g7vjOlDH3LKXQ_harhw&amp;sll=-23.514158,-46.718953&amp;sspn=0.010684,0.021136&amp;g=Rua+Bartolomeu+Paes,+579+-+S%C3%A3o+Paulo+-+SP&amp;mra=mift&amp;ie=UTF8&amp;t=m&amp;ll=-23.514177,-46.718974&amp;spn=0.011806,0.012875&amp;z=15" style="color:#0000FF;text-align:left">Ver mapa maior</a></small> 
                    </div>
                
            </form>

        </div>
       
         
        <?php  
    } 

 
    function renderHeaderMenu()
    {
        global  $menuController;
        $menuController->RenderTopMenu();
    }
 
?>
