<?php
	include "./Master/master.php";  

	function loadObjects()
	{
		global $produtosController;
	    global $produtoController;
		$idProduto = $_GET["idProduto"];
		$idProduto = Util::mysql_real_escape_string($idProduto);

		$produtoController = new ProdutoController($idProduto);	
		$produtosController = new ProdutosController($produtoController->produto->IdCategoria); 
	}
 	function renderTitle()
 	{
 		global $produtoController;
		echo $produtoController->GetTituloPaginaProduto();
 	}

	
	function renderHeaderMenu()
 	{
		$menuController = new MenuController();
		$menuController->RenderTopMenu();
 	}



    function renderMainContent()
    {
    	
	    global $produtosController;
	    global $produtoController;
     	?>
 
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="<?php $_SERVER['REMOTE_HOST'] ?>/home">Home</a><span class="divider">/</span></li> 
				<li><a href="#">Produtos</a><span class="divider">/</span></li>	
				<li><a href="<?php $_SERVER['REMOTE_HOST'] ?>/produtos/<?php echo str_replace(" ", "-", $produtoController->categoria->Nome) . "/" . $produtoController->produto->IdCategoria; ?>">
					<?php echo $produtosController->RenderNomeCategoria();  ?></a><span class="divider">/</span></li>
				<li><a href="#" class="active"><?php echo $produtoController->RenderProdutoCodigo();  ?></a></li>
			</ul>
			<h3><?php $produtoController->RenderProdutoNome();  ?></h3>
			 
			<div class="container">	
				 
				<?php $produtoController->RenderProdutoDetalhes(); ?>
				 
			</div>		
		</div>

        <?php  
    } 

   
?>
