<?php
    include_once "./Util/Util.php";
    include_once "./Controllers/MenuController.php";   
    include_once "./Controllers/HomeController.php"; 
    include_once "./Controllers/InstitucionalController.php";
    include_once "./Controllers/ProdutosController.php";
    include_once "./Controllers/ProdutoController.php";
    include_once "./Models/DestaqueModel.php";
    include_once "./Models/CategoriaProdutoModel.php";
    include_once "./Models/ConfiguracaoModel.php";
    include_once "./Models/ProdutoModel.php";

    $configuracoes = ConfiguracaoModel::CarregarConfiguracoes();
    loadObjects();
?>

<!--[if gt IE 7]>
<!DOCTYPE html> 
<![endif]-->
 <!--[if lt IE 8]>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<![endif]-->
<html lang="pt-BR">
  <head>
    <meta charset="utf-8" /> 
    <title><?php renderTitle(); ?></title>
     <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     <meta name="description" content="<?php renderTitle(); ?>" />
     <meta name="Classification" content="mesas para restaurante; cadeiras para restaurante; bancos para restaurante">
     <meta name="author" content="Raphael Carubbi Neto" />
    <meta name="LANGUAGE" content="Portuguese">
    <meta name="title" content="<?php renderTitle(); ?>" />
    <meta name="url" content="www.teralinemoveis.com.br" />
    <meta name="keywords" content="mesas para restaurante, cadeiras para restaurante e bancos para restaurante" />
    <meta name="company" content="Raphael Carubbi Neto Tecnologia ME" />
    <meta name="ROBOTS" content="index,follow">
    <meta name="revisit-after" content="7 days">
    <meta name="Publisher" content="Raphael Carubbi Neto">
    <meta name="Designer" content="Raphael Carubbi Neto">
    <link rev="made" href="mailto:rcarubbi@gmail.com" />
    <link rel="icon" href="/favicon.ico">
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo $_SERVER['REMOTE_HOST'] ?>/css/<?php  echo  $configuracoes["tema"] ?>/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo $_SERVER['REMOTE_HOST'] ?>/css/bootstrap-responsive.min.css"  />
    <link rel="stylesheet" href="<?php echo $_SERVER['REMOTE_HOST'] ?>/css/application.css"  />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="<?php echo $_SERVER['REMOTE_HOST'] ?>/js/bootstrap.js" type="text/javascript" charset="utf-8"></script>
     
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38053381-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  </head>
  <body>
    
   <?php 
     renderHeaderMenu(); 
     
         renderMainContent();
     ?>
      <footer id="footer">
        <div class="container">
          <div class="row">
            <div class="span4">© 2013 TERA LINE · Todos os direitos Reservados</div>
            <div class="span3"><i class="icon-envelope <?php echo $configuracoes["iconColor"] ?>"></i><a href="mailto:vendas@teraline.com.br">vendas@teraline.com.br</a></div>
            <div class="span3"><a href="tel:1136414661">(11) 3641-4661</a> / <a href="tel:1136421801">3642-1801</a></div>
            <div class="span2"><a class="link pull-right" href="#">Voltar ao Topo</a></div>
          </div>
        </div>
      </footer>
    
  </body>
</html>





