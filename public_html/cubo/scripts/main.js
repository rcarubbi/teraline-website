var main = {
	upFace: $("#cube-up-face table"),
	downface: $("#cube-down-face table"),
	frontFace: $("#cube-front-face table"),
	backFace: $("#cube-back-face table"),
	leftFace: $("#cube-left-face table"),
	rightFace: $("#cube-right-face table"),


	moveAxisX: function(isAntiClockWise)
	{
		main.moveLeft(!isAntiClockWise);
		main.moveStanding(isAntiClockWise);
		main.moveRight(isAntiClockWise);
	},
	moveAxisY: function(isAntiClockWise)
	{
		main.moveUp(isAntiClockWise);
		main.moveEquator(isAntiClockWise);
		main.moveDown(!isAntiClockWise);
	},
	moveAxisZ: function(isAntiClockWise)
	{
		main.moveFront(isAntiClockWise);
		main.moveMiddle(isAntiClockWise);
		main.moveBack(!isAntiClockWise);
	},
	moveRight: function(isAntiClockWise)
	{
		var rightRoullete = main.getXRoullete(1, rightFace);
		if (!isAntiClockWise)
		{	
			rightRoullete = main.rotateXClockwise(rightRoullete, false);
		}
		else
		{
			rightRoullete = main.rotateXCounterClockwise(rightRoullete, false);
		}
		main.setXRoullete(rightRoullete, 1, rightFace);
	},
	moveLeft: function(isAntiClockWise)
	{
		var leftRoullete = main.getXRoullete(3, leftFace);
		if (!isAntiClockWise)
		{
			leftRoullete = main.rotateXCounterClockwise(leftRoullete, true);
			
		}
		else
		{
			leftRoullete = main.rotateXClockwise(leftRoullete, true);
		}
		main.setXRoullete(leftRoullete, 3, leftFace);
	},
	moveUp: function(isAntiClockWise)
	{
		var upRoullete = main.getYRoullete(1, upFace);
		if (!isAntiClockWise)
		{
			upRoullete = main.rotateYClockwise(upRoullete, false);
		}
		else
		{
			upRoullete = main.rotateYCounterClockwise(upRoullete, false);
		}
		main.setYRoullete(upRoullete, 1, upFace);
	},
	moveDown: function(isAntiClockWise)
	{
		var downRoullete = main.getYRoullete(3, downface);
		if (!isAntiClockWise)
		{
			downRoullete = main.rotateYCounterClockwise(downRoullete, true);
		}
		else
		{
			downRoullete = main.rotateYClockwise(downRoullete, true);
			
		}
		main.setYRoullete(downRoullete, 3, downface);
	
	},
	moveFront: function(isAntiClockWise)
	{
		var frontRoullete = main.getZRoullete(3, frontFace);
		if (!isAntiClockWise)
		{
			frontRoullete = main.rotateZClockwise(frontRoullete, false);
		}
		else
		{
			frontRoullete = main.rotateZCounterClockwise(frontRoullete, false);
		}
		main.setZRoullete(frontRoullete, 3, frontFace);

	},
	moveBack: function(isAntiClockWise)
	{
		var backRoullete = main.getZRoullete(1, backFace);
		if (!isAntiClockWise)
		{
				backRoullete = main.rotateZCounterClockwise(backRoullete, true);
		}
		else
		{
				backRoullete = main.rotateZClockwise(backRoullete, true);
			
		}
		main.setZRoullete(backRoullete, 1, backFace);
	},
	moveEquator: function(isAntiClockWise)
	{
		var equatorRoullete = main.getYRoullete(2, null);
		if (!isAntiClockWise)
		{
			equatorRoullete = main.rotateYClockwise(equatorRoullete, null);
		}
		else
		{
			equatorRoullete = main.rotateYCounterClockwise(equatorRoullete, null);
		}
		main.setYRoullete(equatorRoullete, 2, null);
	},
	moveStanding: function(isAntiClockWise)
	{
		var standingRoullete = main.getXRoullete(2, null);
		if (!isAntiClockWise)
		{
			standingRoullete = main.rotateXClockwise(standingRoullete, null);
			
		}
		else
		{
			
			standingRoullete = main.rotateXCounterClockwise(standingRoullete, null);

		}
		main.setXRoullete(standingRoullete, 2, null);

	},
	moveMiddle: function(isAntiClockWise)
	{
		var middleRoullete = main.getZRoullete(2, null);
		if (!isAntiClockWise)
		{
			middleRoullete = main.rotateZClockwise(middleRoullete, null);
		}
		else
		{
			middleRoullete = main.rotateZCounterClockwise(middleRoullete, null);
		}
		main.setZRoullete(middleRoullete, 2, null);
	},
	moveFace: function(alghoritmNotation)
	{
		var isAntiClockWise = alghoritmNotation.indexOf('\'') > -1;
		var letter = alghoritmNotation[0];
		switch(letter)
		{
			case 'x': 
				main.moveAxisX(isAntiClockWise);
				break;
			case 'y':
				main.moveAxisY(isAntiClockWise);
				break;
			case 'z':
				main.moveAxisZ(isAntiClockWise);
				break;
			case "R":
				main.moveRight(isAntiClockWise);
				break;
			case "L":
				main.moveLeft(isAntiClockWise);
				break;
			case "U":
				main.moveUp(isAntiClockWise);
				break;
			case "D":
				main.moveDown(isAntiClockWise);
				break;
			case "F":
				main.moveFront(isAntiClockWise);
				break;
			case "B":
				main.moveBack(isAntiClockWise);
				break;
			case "M":
				main.moveMiddle(isAntiClockWise);
				break;
			case "E":
				main.moveEquator(isAntiClockWise);
				break;
			case "S":
				main.moveStanding(isAntiClockWise);
				break;
			default:
				console.log(alghoritmNotation + " not found");
				break;

		}

	},
	onload: function()
	{
		upFace= $("#cube-up-face table");
		downface=$("#cube-down-face table");
		frontFace= $("#cube-front-face table");
		backFace= $("#cube-back-face table");
		leftFace= $("#cube-left-face table");
		rightFace= $("#cube-right-face table");
		

		$("#commands-container button").button().on("click", function(event)
			{
				event.preventDefault();
				main.moveFace($(this).text());
			});
	},
	getXRoullete: function(rowIndex, lateralFace)
	{
		var roullete = {};
		if (lateralFace)
		{
			roullete.lateralColors = [
			[
				 lateralFace.find("tr:nth-child(1) td:nth-child(1)").css("background-color"),
				 lateralFace.find("tr:nth-child(1) td:nth-child(2)").css("background-color"),
				 lateralFace.find("tr:nth-child(1) td:nth-child(3)").css("background-color")
			],
			[
				 lateralFace.find("tr:nth-child(2) td:nth-child(1)").css("background-color"),
				 lateralFace.find("tr:nth-child(2) td:nth-child(2)").css("background-color"),
				 lateralFace.find("tr:nth-child(2) td:nth-child(3)").css("background-color")
			], 
			[	
				 lateralFace.find("tr:nth-child(3) td:nth-child(1)").css("background-color"),
				 lateralFace.find("tr:nth-child(3) td:nth-child(2)").css("background-color"),
				 lateralFace.find("tr:nth-child(3) td:nth-child(3)").css("background-color")
			]];
		}

		roullete.upColors = [
			 upFace.find("tr:nth-child("+rowIndex+") td:nth-child(1)").css("background-color"),
			 upFace.find("tr:nth-child("+rowIndex+") td:nth-child(2)").css("background-color"),
			 upFace.find("tr:nth-child("+rowIndex+") td:nth-child(3)").css("background-color")
		];

		roullete.downColors = [
			 downface.find("tr:nth-child("+rowIndex+") td:nth-child(1)").css("background-color"),
			 downface.find("tr:nth-child("+rowIndex+") td:nth-child(2)").css("background-color"),
			 downface.find("tr:nth-child("+rowIndex+") td:nth-child(3)").css("background-color")
		];

		roullete.frontColors = [
			 frontFace.find("tr:nth-child("+rowIndex+") td:nth-child(1)").css("background-color"),
			 frontFace.find("tr:nth-child("+rowIndex+") td:nth-child(2)").css("background-color"),
			 frontFace.find("tr:nth-child("+rowIndex+") td:nth-child(3)").css("background-color")
		];

		roullete.backColors = [
			 backFace.find("tr:nth-child("+rowIndex+") td:nth-child(1)").css("background-color"),
			 backFace.find("tr:nth-child("+rowIndex+") td:nth-child(2)").css("background-color"),
			 backFace.find("tr:nth-child("+rowIndex+") td:nth-child(3)").css("background-color")
		];
		return roullete;		
	},
	getYRoullete: function(rowIndex, lateralFace)
	{
		var roullete = {};
		if (lateralFace)
		{
			roullete.lateralColors = [
			[
				 lateralFace.find("tr:nth-child(1) td:nth-child(1)").css("background-color"),
				 lateralFace.find("tr:nth-child(1) td:nth-child(2)").css("background-color"),
				 lateralFace.find("tr:nth-child(1) td:nth-child(3)").css("background-color")
			],
			[
				 lateralFace.find("tr:nth-child(2) td:nth-child(1)").css("background-color"),
				 lateralFace.find("tr:nth-child(2) td:nth-child(2)").css("background-color"),
				 lateralFace.find("tr:nth-child(2) td:nth-child(3)").css("background-color")
			], 
			[	
				 lateralFace.find("tr:nth-child(3) td:nth-child(1)").css("background-color"),
				 lateralFace.find("tr:nth-child(3) td:nth-child(2)").css("background-color"),
				 lateralFace.find("tr:nth-child(3) td:nth-child(3)").css("background-color")
			]];
		}
		var mirrorIndex = ((rowIndex-4)*-1);

		roullete.leftColors = [
			 leftFace.find("tr:nth-child("+rowIndex+") td:nth-child(1)").css("background-color"),
			 leftFace.find("tr:nth-child("+rowIndex+") td:nth-child(2)").css("background-color"),
			 leftFace.find("tr:nth-child("+rowIndex+") td:nth-child(3)").css("background-color")
		];

		roullete.rightColors = [
			 rightFace.find("tr:nth-child("+mirrorIndex+") td:nth-child(1)").css("background-color"),
			 rightFace.find("tr:nth-child("+mirrorIndex+") td:nth-child(2)").css("background-color"),
			 rightFace.find("tr:nth-child("+mirrorIndex+") td:nth-child(3)").css("background-color")
		];

		roullete.frontColors = [
			 frontFace.find("tr:nth-child(1) td:nth-child("+rowIndex+")").css("background-color"),
			 frontFace.find("tr:nth-child(2) td:nth-child("+rowIndex+")").css("background-color"),
			 frontFace.find("tr:nth-child(3) td:nth-child("+rowIndex+")").css("background-color")
		];

		

		roullete.backColors = [
			 backFace.find("tr:nth-child(1) td:nth-child("+mirrorIndex+")").css("background-color"),
			 backFace.find("tr:nth-child(2) td:nth-child("+mirrorIndex+")").css("background-color"),
			 backFace.find("tr:nth-child(3) td:nth-child("+mirrorIndex+")").css("background-color")
		];

		return roullete;		
	},
	getZRoullete : function(rowIndex, lateralFace)
	{
		var roullete = {};
		if (lateralFace)
		{
			roullete.lateralColors = [
			[
				 lateralFace.find("tr:nth-child(1) td:nth-child(1)").css("background-color"),
				 lateralFace.find("tr:nth-child(1) td:nth-child(2)").css("background-color"),
				 lateralFace.find("tr:nth-child(1) td:nth-child(3)").css("background-color")
			],
			[
				 lateralFace.find("tr:nth-child(2) td:nth-child(1)").css("background-color"),
				 lateralFace.find("tr:nth-child(2) td:nth-child(2)").css("background-color"),
				 lateralFace.find("tr:nth-child(2) td:nth-child(3)").css("background-color")
			], 
			[	
				 lateralFace.find("tr:nth-child(3) td:nth-child(1)").css("background-color"),
				 lateralFace.find("tr:nth-child(3) td:nth-child(2)").css("background-color"),
				 lateralFace.find("tr:nth-child(3) td:nth-child(3)").css("background-color")
			]];
		}
		var mirrorIndex = ((rowIndex-4)*-1);

		roullete.leftColors = [
			 leftFace.find("tr:nth-child(1) td:nth-child("+rowIndex+")").css("background-color"),
			 leftFace.find("tr:nth-child(2) td:nth-child("+rowIndex+")").css("background-color"),
			 leftFace.find("tr:nth-child(3) td:nth-child("+rowIndex+")").css("background-color")
		];

		roullete.rightColors = [
			 rightFace.find("tr:nth-child(1) td:nth-child("+rowIndex+")").css("background-color"),
			 rightFace.find("tr:nth-child(2) td:nth-child("+rowIndex+")").css("background-color"),
			 rightFace.find("tr:nth-child(3) td:nth-child("+rowIndex+")").css("background-color")
		];

		roullete.upColors = [
			 upFace.find("tr:nth-child(1) td:nth-child("+rowIndex+")").css("background-color"),
			 upFace.find("tr:nth-child(2) td:nth-child("+rowIndex+")").css("background-color"),
			 upFace.find("tr:nth-child(3) td:nth-child("+rowIndex+")").css("background-color")
		];
		roullete.downColors = [
			 downface.find("tr:nth-child(1) td:nth-child("+mirrorIndex+")").css("background-color"),
			 downface.find("tr:nth-child(2) td:nth-child("+mirrorIndex+")").css("background-color"),
			 downface.find("tr:nth-child(3) td:nth-child("+mirrorIndex+")").css("background-color")
		];

		return roullete;
	},
	setXRoullete: function(roullete, rowIndex, lateralFace)
	{
		if (lateralFace)
		{
			 lateralFace.find("tr:nth-child(1) td:nth-child(1)").css("background-color", roullete.lateralColors[0][0]);
			 lateralFace.find("tr:nth-child(1) td:nth-child(2)").css("background-color", roullete.lateralColors[0][1]);
			 lateralFace.find("tr:nth-child(1) td:nth-child(3)").css("background-color", roullete.lateralColors[0][2]);
			 lateralFace.find("tr:nth-child(2) td:nth-child(1)").css("background-color", roullete.lateralColors[1][0]);
			 lateralFace.find("tr:nth-child(2) td:nth-child(2)").css("background-color", roullete.lateralColors[1][1]);
			 lateralFace.find("tr:nth-child(2) td:nth-child(3)").css("background-color", roullete.lateralColors[1][2]);
			 lateralFace.find("tr:nth-child(3) td:nth-child(1)").css("background-color", roullete.lateralColors[2][0]);
			 lateralFace.find("tr:nth-child(3) td:nth-child(2)").css("background-color", roullete.lateralColors[2][1]);
			 lateralFace.find("tr:nth-child(3) td:nth-child(3)").css("background-color", roullete.lateralColors[2][2]);
		 }


		 upFace.find("tr:nth-child("+rowIndex+") td:nth-child(1)").css("background-color", roullete.upColors[0]);
		 upFace.find("tr:nth-child("+rowIndex+") td:nth-child(2)").css("background-color", roullete.upColors[1]);
		 upFace.find("tr:nth-child("+rowIndex+") td:nth-child(3)").css("background-color", roullete.upColors[2]);
		 
		 backFace.find("tr:nth-child("+rowIndex+") td:nth-child(1)").css("background-color", roullete.backColors[0]);
		 backFace.find("tr:nth-child("+rowIndex+") td:nth-child(2)").css("background-color", roullete.backColors[1]);
		 backFace.find("tr:nth-child("+rowIndex+") td:nth-child(3)").css("background-color", roullete.backColors[2]);

		 frontFace.find("tr:nth-child("+rowIndex+") td:nth-child(1)").css("background-color", roullete.frontColors[0]);
		 frontFace.find("tr:nth-child("+rowIndex+") td:nth-child(2)").css("background-color", roullete.frontColors[1]);
		 frontFace.find("tr:nth-child("+rowIndex+") td:nth-child(3)").css("background-color", roullete.frontColors[2]);

		 downface.find("tr:nth-child("+rowIndex+") td:nth-child(1)").css("background-color", roullete.downColors[0]);
		 downface.find("tr:nth-child("+rowIndex+") td:nth-child(2)").css("background-color", roullete.downColors[1]);
		 downface.find("tr:nth-child("+rowIndex+") td:nth-child(3)").css("background-color", roullete.downColors[2]);


	},
	setYRoullete: function(roullete, rowIndex, lateralFace)
	{
		if (lateralFace)
		{
			 lateralFace.find("tr:nth-child(1) td:nth-child(1)").css("background-color", roullete.lateralColors[0][0]);
			 lateralFace.find("tr:nth-child(1) td:nth-child(2)").css("background-color", roullete.lateralColors[0][1]);
			 lateralFace.find("tr:nth-child(1) td:nth-child(3)").css("background-color", roullete.lateralColors[0][2]);
			 lateralFace.find("tr:nth-child(2) td:nth-child(1)").css("background-color", roullete.lateralColors[1][0]);
			 lateralFace.find("tr:nth-child(2) td:nth-child(2)").css("background-color", roullete.lateralColors[1][1]);
			 lateralFace.find("tr:nth-child(2) td:nth-child(3)").css("background-color", roullete.lateralColors[1][2]);
			 lateralFace.find("tr:nth-child(3) td:nth-child(1)").css("background-color", roullete.lateralColors[2][0]);
			 lateralFace.find("tr:nth-child(3) td:nth-child(2)").css("background-color", roullete.lateralColors[2][1]);
			 lateralFace.find("tr:nth-child(3) td:nth-child(3)").css("background-color", roullete.lateralColors[2][2]);
		 }

 		 var mirrorIndex = ((rowIndex-4)*-1);

		 leftFace.find("tr:nth-child("+rowIndex+") td:nth-child(1)").css("background-color", roullete.leftColors[0]);
		 leftFace.find("tr:nth-child("+rowIndex+") td:nth-child(2)").css("background-color", roullete.leftColors[1]);
		 leftFace.find("tr:nth-child("+rowIndex+") td:nth-child(3)").css("background-color", roullete.leftColors[2]);

		 backFace.find("tr:nth-child(1) td:nth-child("+mirrorIndex+")").css("background-color", roullete.backColors[0]);
		 backFace.find("tr:nth-child(2) td:nth-child("+mirrorIndex+")").css("background-color", roullete.backColors[1]);
		 backFace.find("tr:nth-child(3) td:nth-child("+mirrorIndex+")").css("background-color", roullete.backColors[2]);

		 frontFace.find("tr:nth-child(1) td:nth-child("+rowIndex+")").css("background-color", roullete.frontColors[0]);
		 frontFace.find("tr:nth-child(2) td:nth-child("+rowIndex+")").css("background-color", roullete.frontColors[1]);
		 frontFace.find("tr:nth-child(3) td:nth-child("+rowIndex+")").css("background-color", roullete.frontColors[2]);

		 rightFace.find("tr:nth-child("+mirrorIndex+") td:nth-child(1)").css("background-color", roullete.rightColors[0]);
		 rightFace.find("tr:nth-child("+mirrorIndex+") td:nth-child(2)").css("background-color", roullete.rightColors[1]);
		 rightFace.find("tr:nth-child("+mirrorIndex+") td:nth-child(3)").css("background-color", roullete.rightColors[2]);
	},
	setZRoullete : function(roullete, rowIndex, lateralFace)
	{
		if (lateralFace)
		{
			 lateralFace.find("tr:nth-child(1) td:nth-child(1)").css("background-color", roullete.lateralColors[0][0]);
			 lateralFace.find("tr:nth-child(1) td:nth-child(2)").css("background-color", roullete.lateralColors[0][1]);
			 lateralFace.find("tr:nth-child(1) td:nth-child(3)").css("background-color", roullete.lateralColors[0][2]);
			 lateralFace.find("tr:nth-child(2) td:nth-child(1)").css("background-color", roullete.lateralColors[1][0]);
			 lateralFace.find("tr:nth-child(2) td:nth-child(2)").css("background-color", roullete.lateralColors[1][1]);
			 lateralFace.find("tr:nth-child(2) td:nth-child(3)").css("background-color", roullete.lateralColors[1][2]);
			 lateralFace.find("tr:nth-child(3) td:nth-child(1)").css("background-color", roullete.lateralColors[2][0]);
			 lateralFace.find("tr:nth-child(3) td:nth-child(2)").css("background-color", roullete.lateralColors[2][1]);
			 lateralFace.find("tr:nth-child(3) td:nth-child(3)").css("background-color", roullete.lateralColors[2][2]);
		 }

 		 var mirrorIndex = ((rowIndex-4)*-1);

		 leftFace.find("tr:nth-child(1) td:nth-child("+rowIndex+")").css("background-color", roullete.leftColors[0]);
		 leftFace.find("tr:nth-child(2) td:nth-child("+rowIndex+")").css("background-color", roullete.leftColors[1]);
		 leftFace.find("tr:nth-child(3) td:nth-child("+rowIndex+")").css("background-color", roullete.leftColors[2]);

		 downface.find("tr:nth-child(1) td:nth-child("+mirrorIndex+")").css("background-color", roullete.downColors[0]);
		 downface.find("tr:nth-child(2) td:nth-child("+mirrorIndex+")").css("background-color", roullete.downColors[1]);
		 downface.find("tr:nth-child(3) td:nth-child("+mirrorIndex+")").css("background-color", roullete.downColors[2]);

		 upFace.find("tr:nth-child(1) td:nth-child("+rowIndex+")").css("background-color", roullete.upColors[0]);
		 upFace.find("tr:nth-child(2) td:nth-child("+rowIndex+")").css("background-color", roullete.upColors[1]);
		 upFace.find("tr:nth-child(3) td:nth-child("+rowIndex+")").css("background-color", roullete.upColors[2]);

		 rightFace.find("tr:nth-child(1) td:nth-child("+rowIndex+")").css("background-color", roullete.rightColors[0]);
		 rightFace.find("tr:nth-child(2) td:nth-child("+rowIndex+")").css("background-color", roullete.rightColors[1]);
		 rightFace.find("tr:nth-child(3) td:nth-child("+rowIndex+")").css("background-color", roullete.rightColors[2]);

	},
	rotateLateralFaceClockwise : function(roullete)
	{
			var tempColor = roullete.lateralColors[0][0];
			roullete.lateralColors[0][0] = roullete.lateralColors[2][0];
			roullete.lateralColors[2][0] = roullete.lateralColors[2][2];
			roullete.lateralColors[2][2] = roullete.lateralColors[0][2];
			roullete.lateralColors[0][2] = tempColor;
			
			// permute centers
			tempColor = roullete.lateralColors[0][1];
			roullete.lateralColors[0][1] = roullete.lateralColors[1][0];
			roullete.lateralColors[1][0] = roullete.lateralColors[2][1];
			roullete.lateralColors[2][1] = roullete.lateralColors[1][2];
			roullete.lateralColors[1][2] = tempColor;	
			return roullete.lateralColors;
	},
	rotateLateralFaceCounterClockwise : function(roullete)
	{
			// permute corners
			var tempColor = roullete.lateralColors[0][0];
			roullete.lateralColors[0][0] = roullete.lateralColors[0][2];
			roullete.lateralColors[0][2] = roullete.lateralColors[2][2];
			roullete.lateralColors[2][2] = roullete.lateralColors[2][0];
			roullete.lateralColors[2][0] = tempColor;
			
			// permute centers
			tempColor = roullete.lateralColors[0][1];
			roullete.lateralColors[0][1] = roullete.lateralColors[1][2];
			roullete.lateralColors[1][2] = roullete.lateralColors[2][1];
			roullete.lateralColors[2][1] = roullete.lateralColors[1][0];
			roullete.lateralColors[1][0] = tempColor;	
			return roullete.lateralColors;	

	},
	rotateXClockwise: function(roullete, isLateralInverse)
	{
		   	if (roullete.lateralColors)
		   	{
		   		if (!isLateralInverse)
					roullete.lateralColors = main.rotateLateralFaceClockwise(roullete);
				else
					roullete.lateralColors = main.rotateLateralFaceCounterClockwise(roullete);
			}
			
			var tempColor1 = roullete.upColors[0];
			var tempColor2 = roullete.upColors[1];
			var tempColor3 = roullete.upColors[2];

			roullete.upColors[0] = roullete.frontColors[0];
			roullete.upColors[1] = roullete.frontColors[1];
			roullete.upColors[2] = roullete.frontColors[2];
			
			roullete.frontColors[0] = roullete.downColors[0];
			roullete.frontColors[1] = roullete.downColors[1];
			roullete.frontColors[2] = roullete.downColors[2];

			roullete.downColors[0] = roullete.backColors[0];
			roullete.downColors[1] = roullete.backColors[1];
			roullete.downColors[2] = roullete.backColors[2];

			roullete.backColors[0] = tempColor1;
			roullete.backColors[1] = tempColor2;
			roullete.backColors[2] = tempColor3;

			return roullete;
	},
	rotateXCounterClockwise : function(roullete, isLateralInverse){
			if (roullete.lateralColors)
			{
				if (!isLateralInverse)
					roullete.lateralColors = main.rotateLateralFaceCounterClockwise(roullete);
				else
					roullete.lateralColors = main.rotateLateralFaceClockwise(roullete);
			}	

			var tempColor1 = roullete.upColors[0];
			var tempColor2 = roullete.upColors[1];
			var tempColor3 = roullete.upColors[2];
			roullete.upColors[0] = roullete.backColors[0];
			roullete.upColors[1] = roullete.backColors[1];
			roullete.upColors[2] = roullete.backColors[2];
			
			roullete.backColors[0] = roullete.downColors[0];
			roullete.backColors[1] = roullete.downColors[1];
			roullete.backColors[2] = roullete.downColors[2];

			roullete.downColors[0] = roullete.frontColors[0];
			roullete.downColors[1] = roullete.frontColors[1];
			roullete.downColors[2] = roullete.frontColors[2];

			roullete.frontColors[0] = tempColor1;
			roullete.frontColors[1] = tempColor2;
			roullete.frontColors[2] = tempColor3;

			return roullete;
	},
	rotateYClockwise: function(roullete, isLateralInverse) {
			if (roullete.lateralColors)
			{
				if (!isLateralInverse)
					roullete.lateralColors = main.rotateLateralFaceClockwise(roullete);
				else
					roullete.lateralColors = main.rotateLateralFaceCounterClockwise(roullete);
			}	

			var tempColor1 = roullete.frontColors[0];
			var tempColor2 = roullete.frontColors[1];
			var tempColor3 = roullete.frontColors[2];
			roullete.frontColors[0] = roullete.rightColors[0];
			roullete.frontColors[1] = roullete.rightColors[1];
			roullete.frontColors[2] = roullete.rightColors[2];
			
			roullete.rightColors[0] = roullete.backColors[2];
			roullete.rightColors[1] = roullete.backColors[1];
			roullete.rightColors[2] = roullete.backColors[0];

			roullete.backColors[0] = roullete.leftColors[0];
			roullete.backColors[1] = roullete.leftColors[1];
			roullete.backColors[2] = roullete.leftColors[2];

			roullete.leftColors[0] = tempColor3;
			roullete.leftColors[1] = tempColor2;
			roullete.leftColors[2] = tempColor1;

			return roullete;	
	},
	rotateYCounterClockwise : function(roullete, isLateralInverse){
		if (roullete.lateralColors)
			{
				if (!isLateralInverse)
					roullete.lateralColors = main.rotateLateralFaceCounterClockwise(roullete);
				else
					roullete.lateralColors = main.rotateLateralFaceClockwise(roullete);
			}	

			var tempColor1 = roullete.frontColors[0];
			var tempColor2 = roullete.frontColors[1];
			var tempColor3 = roullete.frontColors[2];
			roullete.frontColors[0] = roullete.leftColors[2];
			roullete.frontColors[1] = roullete.leftColors[1];
			roullete.frontColors[2] = roullete.leftColors[0];
			
			roullete.leftColors[0] = roullete.backColors[0];
			roullete.leftColors[1] = roullete.backColors[1];
			roullete.leftColors[2] = roullete.backColors[2];

			roullete.backColors[0] = roullete.rightColors[2];
			roullete.backColors[1] = roullete.rightColors[1];
			roullete.backColors[2] = roullete.rightColors[0];

			roullete.rightColors[0] = tempColor1;
			roullete.rightColors[1] = tempColor2;
			roullete.rightColors[2] = tempColor3;

			return roullete;	
	},
	rotateZClockwise: function(roullete, isLateralInverse){
			if (roullete.lateralColors)
			{
				if (!isLateralInverse)
					roullete.lateralColors = main.rotateLateralFaceCounterClockwise(roullete);
				else
					roullete.lateralColors = main.rotateLateralFaceClockwise(roullete);
			}	

			var tempColor1 = roullete.upColors[0];
			var tempColor2 = roullete.upColors[1];
			var tempColor3 = roullete.upColors[2];

			roullete.upColors[0] = roullete.leftColors[0];
			roullete.upColors[1] = roullete.leftColors[1];
			roullete.upColors[2] = roullete.leftColors[2];
			
			roullete.leftColors[0] = roullete.downColors[2];
			roullete.leftColors[1] = roullete.downColors[1];
			roullete.leftColors[2] = roullete.downColors[0];

			roullete.downColors[0] = roullete.rightColors[2];
			roullete.downColors[1] = roullete.rightColors[1];
			roullete.downColors[2] = roullete.rightColors[0];

			roullete.rightColors[0] = tempColor1;
			roullete.rightColors[1] = tempColor2;
			roullete.rightColors[2] = tempColor3;

			return roullete;

	},
	rotateZCounterClockwise: function(roullete, isLateralInverse){

			if (roullete.lateralColors)
			{
				if (!isLateralInverse)
					roullete.lateralColors = main.rotateLateralFaceClockwise(roullete);
				else
					roullete.lateralColors = main.rotateLateralFaceCounterClockwise(roullete);
			}	

			var tempColor1 = roullete.upColors[0];
			var tempColor2 = roullete.upColors[1];
			var tempColor3 = roullete.upColors[2];
			
			roullete.upColors[0] = roullete.rightColors[0];
			roullete.upColors[1] = roullete.rightColors[1];
			roullete.upColors[2] = roullete.rightColors[2];
			
			roullete.rightColors[0] = roullete.downColors[2];
			roullete.rightColors[1] = roullete.downColors[1];
			roullete.rightColors[2] = roullete.downColors[0];

			roullete.downColors[0] = roullete.leftColors[2];
			roullete.downColors[1] = roullete.leftColors[1];
			roullete.downColors[2] = roullete.leftColors[0];

			roullete.leftColors[0] = tempColor1;
			roullete.leftColors[1] = tempColor2;
			roullete.leftColors[2] = tempColor3;

			return roullete;
    }

}

window.onload = main.onload.bind(main);